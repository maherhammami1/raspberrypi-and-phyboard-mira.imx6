#ifndef _DATE_       // Avoid multiple inclusion.
#define _DATE_

class Date
{
	private:
		short month, day, year;         // Sheltered members:

	public:                             // Public interface:
		void init(void);
		void init( int month, int day, int year);
		void print(void);
};
#endif                                  //_DATE_
