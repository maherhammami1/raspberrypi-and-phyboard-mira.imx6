// array.cpp
// To input numbers into an array and output after.
// ----------------------------------------------------
#include <iostream>
#include <iomanip>

using namespace std;

#define MAX 100

long number[MAX];

int main()
{	
	int i,cnt;
	
	cout << "\n----------SORTING INTEGERS ----------\n" << endl;            //This is the header of the output program
	cout << "\nEnter your numbers please and it needs to be insorted list\n" << endl;         // message to enter insorted list
	cout << "\nThis program will be shutdown if you enter a letter\n" << endl;
	
	for(i=0;i<MAX && cin>>number[i];i++)
		;
	cnt = i;
	
	/*----------CONTROLING THE NUMBER OF INTEGERS----------------*/
	
	if (cnt==MAX)
		cout << "\n you successfuly enter the right number\n" << endl;
	else
		cout << "\n oooooops you enter only " << cnt << " number\n"<< endl;
	
	/*---------- START SORTING INTEGERS--------------------*/
	
	bool sorted = false;                             // Not yet sorted.
	long help;                                       // Swap.
	int end = cnt;                                   // End of a loop.
	while( !sorted)
	{
		sorted = true;
		--end;
		for( i = 0; i < end; ++i)
		{
			if( number[i] > number[i+1])
			{
				sorted = false;
				help = number[i];
				number[i] = number[i+1];
				number[i+1]= help;
			}
		}
	}
	/*----------Outputs the numbers----------------*/
	
	cout << "The sorted numbers:\n" << endl;
	for( i = 0; i < cnt; ++i)
		cout << setw(10) << number[i] ;
		cout << endl;
		
	return 0;
}
