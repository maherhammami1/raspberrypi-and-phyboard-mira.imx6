FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SUMMARY =  "This image is designed to show development of camera and Qt application \
	    imaging application with openCV and QT \
            IoT application with iotivity and QT \
            HTML5 application \
            running on the eglfs single application backend."

IMAGE_INSTALL += "\
    actia-server \
    nodejs \
    nodejs-npm \
    mraa \
    mraa-utils \
    node-mraa \
    upm \
    node-upm \
    \
    html5demo \
    opencv \
    gstreamer1.0-plugins-bad-opencv \
    packagegroup-iotivity \
    ${@bb.utils.contains('DISTRO_FEATURES', 'wayland', 'qtwayland qtwayland-plugins weston weston-init', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'x11 wayland', 'weston-xwayland', '', d)} \
"
IMAGE_INSTALL_remove_mx6ul = "\
    qt5-opengles2-test \
"
