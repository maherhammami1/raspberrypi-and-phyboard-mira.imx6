#
# This file was derived from the 'ACTIA-QTclient' example recipe in the
# Yocto Project Development Manual.
#

PR = "r1"
SUMMARY = "actia Qtclient"
DESCRIPTION = "actia Qtclient application for Edison which demonstrates actia Qtclient capabilities through the integration of an add-on breadboard that hosts temperature, pression, humidity, Gaz and LED resources"
HOMEPAGE = "https://www.iotivity.org/"
DEPENDS = "iotivity mraa"
SECTION = "Qtclient"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://actia-qtclient.tar.gz \
	  "

DEPENDS = "qtbase qtdeclarative"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit pkgconfig qmake5 systemd

TARGET_CC_ARCH += "${LDFLAGS}"

RDEPENDS_${PN} += "\
    qtmultimedia-qmlplugins \
    qtvirtualkeyboard \
    qtquickcontrols2-qmlplugins \
    gstreamer1.0-libav \
    gstreamer1.0-plugins-base-audioconvert \
    gstreamer1.0-plugins-base-audioresample \
    gstreamer1.0-plugins-base-playback \
    gstreamer1.0-plugins-base-typefindfunctions \
    gstreamer1.0-plugins-base-videoconvert \
    gstreamer1.0-plugins-base-videoscale \
    gstreamer1.0-plugins-base-volume \
    gstreamer1.0-plugins-base-vorbis \
    gstreamer1.0-plugins-good-autodetect \
    gstreamer1.0-plugins-good-matroska \
    gstreamer1.0-plugins-good-ossaudio \
    gstreamer1.0-plugins-good-videofilter \
"

S = "${WORKDIR}/actia-qtclient/actia-qtclient"


IOTIVITY_BIN_DIR = "/opt/iotivity"
IOTIVITY_BIN_DIR_D = "${D}${IOTIVITY_BIN_DIR}"

do_compile_prepend() {
    export PKG_CONFIG_PATH="${PKG_CONFIG_PATH}"
    export PKG_CONFIG="PKG_CONFIG_SYSROOT_DIR=\"${PKG_CONFIG_SYSROOT_DIR}\" pkg-config"
    export LD_FLAGS="${LD_FLAGS}"
}


do_install() {
    install -d ${IOTIVITY_BIN_DIR_D}/apps/actia-qtclient
    install -c -m 555 ${S}/client ${IOTIVITY_BIN_DIR_D}/apps/actia-qtclient
}

FILES_${PN} = "${IOTIVITY_BIN_DIR}/apps/actia-qtclient/client" 
FILES_${PN}-dbg = "${IOTIVITY_BIN_DIR}/apps/actia-qtclient/.debug"
RDEPENDS_${PN} += "iotivity-resource mraa"
BBCLASSEXTEND = "native nativesdk"
PACKAGE_DEBUG_SPLIT_STYLE = "debug-without-src"

#
#do_compile() {
#   oe_runmake -C ${WORKDIR}/actia-Qtclient/ all
#}
#
