/****************************************************************************
** Meta object code from reading C++ file 'client.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../actia-qtclient/actia-qtclient/client.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'client.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_LED_t {
    QByteArrayData data[6];
    char stringdata0[45];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LED_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LED_t qt_meta_stringdata_LED = {
    {
QT_MOC_LITERAL(0, 0, 3), // "LED"
QT_MOC_LITERAL(1, 4, 3), // "get"
QT_MOC_LITERAL(2, 8, 0), // ""
QT_MOC_LITERAL(3, 9, 3), // "put"
QT_MOC_LITERAL(4, 13, 22), // "shared_ptr<OCResource>"
QT_MOC_LITERAL(5, 36, 8) // "Resource"

    },
    "LED\0get\0\0put\0shared_ptr<OCResource>\0"
    "Resource"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LED[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       1,   31, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x02 /* Public */,
       3,    1,   25,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,

 // constructors: parameters
    0x80000000 | 2, 0x80000000 | 4,    5,

 // constructors: name, argc, parameters, tag, flags
       0,    1,   28,    2, 0x0e /* Public */,

       0        // eod
};

void LED::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::CreateInstance) {
        switch (_id) {
        case 0: { LED *_r = new LED((*reinterpret_cast< shared_ptr<OCResource>(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast<QObject**>(_a[0]) = _r; } break;
        default: break;
        }
    } else if (_c == QMetaObject::InvokeMetaMethod) {
        LED *_t = static_cast<LED *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->get(); break;
        case 1: _t->put((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject LED::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_LED.data,
      qt_meta_data_LED,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *LED::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LED::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LED.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int LED::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_LEDNF_t {
    QByteArrayData data[6];
    char stringdata0[47];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LEDNF_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LEDNF_t qt_meta_stringdata_LEDNF = {
    {
QT_MOC_LITERAL(0, 0, 5), // "LEDNF"
QT_MOC_LITERAL(1, 6, 3), // "get"
QT_MOC_LITERAL(2, 10, 0), // ""
QT_MOC_LITERAL(3, 11, 3), // "put"
QT_MOC_LITERAL(4, 15, 22), // "shared_ptr<OCResource>"
QT_MOC_LITERAL(5, 38, 8) // "Resource"

    },
    "LEDNF\0get\0\0put\0shared_ptr<OCResource>\0"
    "Resource"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LEDNF[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       1,   31, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x02 /* Public */,
       3,    1,   25,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,

 // constructors: parameters
    0x80000000 | 2, 0x80000000 | 4,    5,

 // constructors: name, argc, parameters, tag, flags
       0,    1,   28,    2, 0x0e /* Public */,

       0        // eod
};

void LEDNF::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::CreateInstance) {
        switch (_id) {
        case 0: { LEDNF *_r = new LEDNF((*reinterpret_cast< shared_ptr<OCResource>(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast<QObject**>(_a[0]) = _r; } break;
        default: break;
        }
    } else if (_c == QMetaObject::InvokeMetaMethod) {
        LEDNF *_t = static_cast<LEDNF *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->get(); break;
        case 1: _t->put((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject LEDNF::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_LEDNF.data,
      qt_meta_data_LEDNF,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *LEDNF::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LEDNF::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LEDNF.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int LEDNF::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_LEDD_t {
    QByteArrayData data[6];
    char stringdata0[46];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LEDD_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LEDD_t qt_meta_stringdata_LEDD = {
    {
QT_MOC_LITERAL(0, 0, 4), // "LEDD"
QT_MOC_LITERAL(1, 5, 3), // "get"
QT_MOC_LITERAL(2, 9, 0), // ""
QT_MOC_LITERAL(3, 10, 3), // "put"
QT_MOC_LITERAL(4, 14, 22), // "shared_ptr<OCResource>"
QT_MOC_LITERAL(5, 37, 8) // "Resource"

    },
    "LEDD\0get\0\0put\0shared_ptr<OCResource>\0"
    "Resource"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LEDD[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       1,   31, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x02 /* Public */,
       3,    1,   25,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,

 // constructors: parameters
    0x80000000 | 2, 0x80000000 | 4,    5,

 // constructors: name, argc, parameters, tag, flags
       0,    1,   28,    2, 0x0e /* Public */,

       0        // eod
};

void LEDD::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::CreateInstance) {
        switch (_id) {
        case 0: { LEDD *_r = new LEDD((*reinterpret_cast< shared_ptr<OCResource>(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast<QObject**>(_a[0]) = _r; } break;
        default: break;
        }
    } else if (_c == QMetaObject::InvokeMetaMethod) {
        LEDD *_t = static_cast<LEDD *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->get(); break;
        case 1: _t->put((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject LEDD::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_LEDD.data,
      qt_meta_data_LEDD,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *LEDD::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LEDD::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LEDD.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int LEDD::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_LEDG_t {
    QByteArrayData data[6];
    char stringdata0[46];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LEDG_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LEDG_t qt_meta_stringdata_LEDG = {
    {
QT_MOC_LITERAL(0, 0, 4), // "LEDG"
QT_MOC_LITERAL(1, 5, 3), // "get"
QT_MOC_LITERAL(2, 9, 0), // ""
QT_MOC_LITERAL(3, 10, 3), // "put"
QT_MOC_LITERAL(4, 14, 22), // "shared_ptr<OCResource>"
QT_MOC_LITERAL(5, 37, 8) // "Resource"

    },
    "LEDG\0get\0\0put\0shared_ptr<OCResource>\0"
    "Resource"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LEDG[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       1,   31, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x02 /* Public */,
       3,    1,   25,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,

 // constructors: parameters
    0x80000000 | 2, 0x80000000 | 4,    5,

 // constructors: name, argc, parameters, tag, flags
       0,    1,   28,    2, 0x0e /* Public */,

       0        // eod
};

void LEDG::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::CreateInstance) {
        switch (_id) {
        case 0: { LEDG *_r = new LEDG((*reinterpret_cast< shared_ptr<OCResource>(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast<QObject**>(_a[0]) = _r; } break;
        default: break;
        }
    } else if (_c == QMetaObject::InvokeMetaMethod) {
        LEDG *_t = static_cast<LEDG *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->get(); break;
        case 1: _t->put((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject LEDG::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_LEDG.data,
      qt_meta_data_LEDG,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *LEDG::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LEDG::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LEDG.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int LEDG::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_LEDGD_t {
    QByteArrayData data[6];
    char stringdata0[47];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LEDGD_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LEDGD_t qt_meta_stringdata_LEDGD = {
    {
QT_MOC_LITERAL(0, 0, 5), // "LEDGD"
QT_MOC_LITERAL(1, 6, 3), // "put"
QT_MOC_LITERAL(2, 10, 0), // ""
QT_MOC_LITERAL(3, 11, 3), // "get"
QT_MOC_LITERAL(4, 15, 22), // "shared_ptr<OCResource>"
QT_MOC_LITERAL(5, 38, 8) // "Resource"

    },
    "LEDGD\0put\0\0get\0shared_ptr<OCResource>\0"
    "Resource"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LEDGD[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       1,   31, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    1,   24,    2, 0x02 /* Public */,
       3,    0,   27,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,

 // constructors: parameters
    0x80000000 | 2, 0x80000000 | 4,    5,

 // constructors: name, argc, parameters, tag, flags
       0,    1,   28,    2, 0x0e /* Public */,

       0        // eod
};

void LEDGD::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::CreateInstance) {
        switch (_id) {
        case 0: { LEDGD *_r = new LEDGD((*reinterpret_cast< shared_ptr<OCResource>(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast<QObject**>(_a[0]) = _r; } break;
        default: break;
        }
    } else if (_c == QMetaObject::InvokeMetaMethod) {
        LEDGD *_t = static_cast<LEDGD *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->put((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->get(); break;
        default: ;
        }
    }
}

const QMetaObject LEDGD::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_LEDGD.data,
      qt_meta_data_LEDGD,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *LEDGD::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LEDGD::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LEDGD.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int LEDGD::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_TemperatureSensor_t {
    QByteArrayData data[4];
    char stringdata0[38];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TemperatureSensor_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TemperatureSensor_t qt_meta_stringdata_TemperatureSensor = {
    {
QT_MOC_LITERAL(0, 0, 17), // "TemperatureSensor"
QT_MOC_LITERAL(1, 18, 12), // "valueChanged"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 5) // "value"

    },
    "TemperatureSensor\0valueChanged\0\0value"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TemperatureSensor[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       1,   20, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,

 // properties: name, type, flags
       3, QMetaType::QString, 0x00495003,

 // properties: notify_signal_id
       0,

       0        // eod
};

void TemperatureSensor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TemperatureSensor *_t = static_cast<TemperatureSensor *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->valueChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (TemperatureSensor::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TemperatureSensor::valueChanged)) {
                *result = 0;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        TemperatureSensor *_t = static_cast<TemperatureSensor *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->value(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        TemperatureSensor *_t = static_cast<TemperatureSensor *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setvalue(*reinterpret_cast< QString*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject TemperatureSensor::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_TemperatureSensor.data,
      qt_meta_data_TemperatureSensor,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *TemperatureSensor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TemperatureSensor::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TemperatureSensor.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int TemperatureSensor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void TemperatureSensor::valueChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
struct qt_meta_stringdata_HumiditySensor_t {
    QByteArrayData data[7];
    char stringdata0[77];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_HumiditySensor_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_HumiditySensor_t qt_meta_stringdata_HumiditySensor = {
    {
QT_MOC_LITERAL(0, 0, 14), // "HumiditySensor"
QT_MOC_LITERAL(1, 15, 3), // "get"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 12), // "startObserve"
QT_MOC_LITERAL(4, 33, 11), // "stopObserve"
QT_MOC_LITERAL(5, 45, 22), // "shared_ptr<OCResource>"
QT_MOC_LITERAL(6, 68, 8) // "Resource"

    },
    "HumiditySensor\0get\0\0startObserve\0"
    "stopObserve\0shared_ptr<OCResource>\0"
    "Resource"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_HumiditySensor[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       1,   35, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x02 /* Public */,
       3,    0,   30,    2, 0x02 /* Public */,
       4,    0,   31,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // constructors: parameters
    0x80000000 | 2, 0x80000000 | 5,    6,

 // constructors: name, argc, parameters, tag, flags
       0,    1,   32,    2, 0x0e /* Public */,

       0        // eod
};

void HumiditySensor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::CreateInstance) {
        switch (_id) {
        case 0: { HumiditySensor *_r = new HumiditySensor((*reinterpret_cast< shared_ptr<OCResource>(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast<QObject**>(_a[0]) = _r; } break;
        default: break;
        }
    } else if (_c == QMetaObject::InvokeMetaMethod) {
        HumiditySensor *_t = static_cast<HumiditySensor *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->get(); break;
        case 1: _t->startObserve(); break;
        case 2: _t->stopObserve(); break;
        default: ;
        }
    }
}

const QMetaObject HumiditySensor::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_HumiditySensor.data,
      qt_meta_data_HumiditySensor,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *HumiditySensor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *HumiditySensor::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_HumiditySensor.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int HumiditySensor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
struct qt_meta_stringdata_PressionSensor_t {
    QByteArrayData data[7];
    char stringdata0[77];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PressionSensor_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PressionSensor_t qt_meta_stringdata_PressionSensor = {
    {
QT_MOC_LITERAL(0, 0, 14), // "PressionSensor"
QT_MOC_LITERAL(1, 15, 3), // "get"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 12), // "startObserve"
QT_MOC_LITERAL(4, 33, 11), // "stopObserve"
QT_MOC_LITERAL(5, 45, 22), // "shared_ptr<OCResource>"
QT_MOC_LITERAL(6, 68, 8) // "Resource"

    },
    "PressionSensor\0get\0\0startObserve\0"
    "stopObserve\0shared_ptr<OCResource>\0"
    "Resource"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PressionSensor[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       1,   35, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x02 /* Public */,
       3,    0,   30,    2, 0x02 /* Public */,
       4,    0,   31,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // constructors: parameters
    0x80000000 | 2, 0x80000000 | 5,    6,

 // constructors: name, argc, parameters, tag, flags
       0,    1,   32,    2, 0x0e /* Public */,

       0        // eod
};

void PressionSensor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::CreateInstance) {
        switch (_id) {
        case 0: { PressionSensor *_r = new PressionSensor((*reinterpret_cast< shared_ptr<OCResource>(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast<QObject**>(_a[0]) = _r; } break;
        default: break;
        }
    } else if (_c == QMetaObject::InvokeMetaMethod) {
        PressionSensor *_t = static_cast<PressionSensor *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->get(); break;
        case 1: _t->startObserve(); break;
        case 2: _t->stopObserve(); break;
        default: ;
        }
    }
}

const QMetaObject PressionSensor::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_PressionSensor.data,
      qt_meta_data_PressionSensor,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *PressionSensor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PressionSensor::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PressionSensor.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int PressionSensor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
struct qt_meta_stringdata_GazSensor_t {
    QByteArrayData data[7];
    char stringdata0[72];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_GazSensor_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_GazSensor_t qt_meta_stringdata_GazSensor = {
    {
QT_MOC_LITERAL(0, 0, 9), // "GazSensor"
QT_MOC_LITERAL(1, 10, 3), // "get"
QT_MOC_LITERAL(2, 14, 0), // ""
QT_MOC_LITERAL(3, 15, 12), // "startObserve"
QT_MOC_LITERAL(4, 28, 11), // "stopObserve"
QT_MOC_LITERAL(5, 40, 22), // "shared_ptr<OCResource>"
QT_MOC_LITERAL(6, 63, 8) // "Resource"

    },
    "GazSensor\0get\0\0startObserve\0stopObserve\0"
    "shared_ptr<OCResource>\0Resource"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_GazSensor[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       1,   35, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x02 /* Public */,
       3,    0,   30,    2, 0x02 /* Public */,
       4,    0,   31,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // constructors: parameters
    0x80000000 | 2, 0x80000000 | 5,    6,

 // constructors: name, argc, parameters, tag, flags
       0,    1,   32,    2, 0x0e /* Public */,

       0        // eod
};

void GazSensor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::CreateInstance) {
        switch (_id) {
        case 0: { GazSensor *_r = new GazSensor((*reinterpret_cast< shared_ptr<OCResource>(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast<QObject**>(_a[0]) = _r; } break;
        default: break;
        }
    } else if (_c == QMetaObject::InvokeMetaMethod) {
        GazSensor *_t = static_cast<GazSensor *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->get(); break;
        case 1: _t->startObserve(); break;
        case 2: _t->stopObserve(); break;
        default: ;
        }
    }
}

const QMetaObject GazSensor::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_GazSensor.data,
      qt_meta_data_GazSensor,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *GazSensor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *GazSensor::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_GazSensor.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int GazSensor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
struct qt_meta_stringdata_IoTClient_t {
    QByteArrayData data[20];
    char stringdata0[285];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_IoTClient_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_IoTClient_t qt_meta_stringdata_IoTClient = {
    {
QT_MOC_LITERAL(0, 0, 9), // "IoTClient"
QT_MOC_LITERAL(1, 10, 14), // "getPlatformLED"
QT_MOC_LITERAL(2, 25, 15), // "shared_ptr<LED>"
QT_MOC_LITERAL(3, 41, 0), // ""
QT_MOC_LITERAL(4, 42, 16), // "getPlatformLEDNF"
QT_MOC_LITERAL(5, 59, 17), // "shared_ptr<LEDNF>"
QT_MOC_LITERAL(6, 77, 15), // "getPlatformLEDD"
QT_MOC_LITERAL(7, 93, 16), // "shared_ptr<LEDD>"
QT_MOC_LITERAL(8, 110, 15), // "getPlatformLEDG"
QT_MOC_LITERAL(9, 126, 16), // "shared_ptr<LEDG>"
QT_MOC_LITERAL(10, 143, 16), // "getPlatformLEDGD"
QT_MOC_LITERAL(11, 160, 17), // "shared_ptr<LEDGD>"
QT_MOC_LITERAL(12, 178, 12), // "findResource"
QT_MOC_LITERAL(13, 191, 14), // "gettemperature"
QT_MOC_LITERAL(14, 206, 11), // "putledonfar"
QT_MOC_LITERAL(15, 218, 8), // "putledon"
QT_MOC_LITERAL(16, 227, 9), // "putledoff"
QT_MOC_LITERAL(17, 237, 16), // "putledclinodroit"
QT_MOC_LITERAL(18, 254, 12), // "putfourclino"
QT_MOC_LITERAL(19, 267, 17) // "putledclinogauche"

    },
    "IoTClient\0getPlatformLED\0shared_ptr<LED>\0"
    "\0getPlatformLEDNF\0shared_ptr<LEDNF>\0"
    "getPlatformLEDD\0shared_ptr<LEDD>\0"
    "getPlatformLEDG\0shared_ptr<LEDG>\0"
    "getPlatformLEDGD\0shared_ptr<LEDGD>\0"
    "findResource\0gettemperature\0putledonfar\0"
    "putledon\0putledoff\0putledclinodroit\0"
    "putfourclino\0putledclinogauche"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_IoTClient[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    0,   79,    3, 0x02 /* Public */,
       4,    0,   80,    3, 0x02 /* Public */,
       6,    0,   81,    3, 0x02 /* Public */,
       8,    0,   82,    3, 0x02 /* Public */,
      10,    0,   83,    3, 0x02 /* Public */,
      12,    0,   84,    3, 0x02 /* Public */,
      13,    0,   85,    3, 0x02 /* Public */,
      14,    0,   86,    3, 0x02 /* Public */,
      15,    0,   87,    3, 0x02 /* Public */,
      16,    0,   88,    3, 0x02 /* Public */,
      17,    0,   89,    3, 0x02 /* Public */,
      18,    0,   90,    3, 0x02 /* Public */,
      19,    0,   91,    3, 0x02 /* Public */,

 // methods: parameters
    0x80000000 | 2,
    0x80000000 | 5,
    0x80000000 | 7,
    0x80000000 | 9,
    0x80000000 | 11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void IoTClient::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        IoTClient *_t = static_cast<IoTClient *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { shared_ptr<LED> _r = _t->getPlatformLED();
            if (_a[0]) *reinterpret_cast< shared_ptr<LED>*>(_a[0]) = std::move(_r); }  break;
        case 1: { shared_ptr<LEDNF> _r = _t->getPlatformLEDNF();
            if (_a[0]) *reinterpret_cast< shared_ptr<LEDNF>*>(_a[0]) = std::move(_r); }  break;
        case 2: { shared_ptr<LEDD> _r = _t->getPlatformLEDD();
            if (_a[0]) *reinterpret_cast< shared_ptr<LEDD>*>(_a[0]) = std::move(_r); }  break;
        case 3: { shared_ptr<LEDG> _r = _t->getPlatformLEDG();
            if (_a[0]) *reinterpret_cast< shared_ptr<LEDG>*>(_a[0]) = std::move(_r); }  break;
        case 4: { shared_ptr<LEDGD> _r = _t->getPlatformLEDGD();
            if (_a[0]) *reinterpret_cast< shared_ptr<LEDGD>*>(_a[0]) = std::move(_r); }  break;
        case 5: _t->findResource(); break;
        case 6: _t->gettemperature(); break;
        case 7: _t->putledonfar(); break;
        case 8: _t->putledon(); break;
        case 9: _t->putledoff(); break;
        case 10: _t->putledclinodroit(); break;
        case 11: _t->putfourclino(); break;
        case 12: _t->putledclinogauche(); break;
        default: ;
        }
    }
}

const QMetaObject IoTClient::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_IoTClient.data,
      qt_meta_data_IoTClient,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *IoTClient::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *IoTClient::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_IoTClient.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int IoTClient::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
