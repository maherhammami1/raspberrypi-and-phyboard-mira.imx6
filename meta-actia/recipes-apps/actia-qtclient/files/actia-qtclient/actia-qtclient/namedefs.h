//******************************************************************
//
// Copyright 2014 Intel Corporation.
//
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

#ifndef NAMEDEFS_H_
#define NAMEDEFS_H_

#define EDISON_RESOURCE_INTERFACE "oic.if.rw"
#define TEMPERATURE_RESOURCE_TYPE "core.temperature"
#define LED_RESOURCE_TYPE "core.led"
#define LEDNF_RESOURCE_TYPE "core.lednf"
#define LEDD_RESOURCE_TYPE "core.ledd"
#define LEDG_RESOURCE_TYPE "core.ledg"
#define LEDGD_RESOURCE_TYPE "core.ledgd"
#define TEMPERATURE_RESOURCE_ENDPOINT "/temperature"
#define LED_RESOURCE_ENDPOINT "/led"
#define LEDNF_RESOURCE_ENDPOINT "/lednf"
#define LEDD_RESOURCE_ENDPOINT "/ledd"
#define LEDG_RESOURCE_ENDPOINT "/ledg"
#define LEDGD_RESOURCE_ENDPOINT "/ledgd"
#define TEMPERATURE_RESOURCE_KEY "temperature"
#define LED_RESOURCE_KEY "switch"
#define LEDNF_RESOURCE_KEY "switch"
#define LEDD_RESOURCE_KEY "switch"
#define LEDG_RESOURCE_KEY "switch"
#define LEDGD_RESOURCE_KEY "switch"

#endif /* NAMEDEFS_H_ */
