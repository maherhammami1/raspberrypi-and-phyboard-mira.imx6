#include "SysFsLed.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

SysFsLed::SysFsLed( const char* sysFsFile )
{
    m_sysFsFile = new char[strlen(sysFsFile) + 1];
    strcpy( m_sysFsFile, sysFsFile );
}


SysFsLed::~SysFsLed()
{
    delete[] m_sysFsFile;
}


int SysFsLed::setLed( int value )
{
    FILE * pFile;
    int retVal = -1;

    if( value > 255 )
    {
        fprintf(stderr, "%s: Warning: value out of range! (%i)", __FUNCTION__, value );
        value = 255;
    }
    if( value < 0 )
    {
        fprintf(stderr, "%s: Warning: value out of range! (%i)", __FUNCTION__, value );
        value = 0;
    }

    pFile = fopen( m_sysFsFile , "r+" );
    if (pFile == NULL)
    {
        perror ("Error opening file");
    }
    else
    {
        char buffer[16];
        snprintf(buffer, sizeof(buffer), "%i\n",value);
        size_t bytesWritten = fwrite( buffer, 1, sizeof(buffer), pFile );
        if( bytesWritten > 0 )
        {
            retVal = 0;
        }
        else
        {
            perror( "Error writing to file" );
        }
        fclose (pFile);
    }
    return retVal;
}


int SysFsLed::getLed()
{
    FILE * pFile;
    int retVal = -1;
    char buffer [32];

    pFile = fopen( m_sysFsFile , "r" );
    if (pFile == NULL)
    {
        perror ("Error opening file");
    }
    else
    {
        size_t bytesRead = fread( buffer, 1, sizeof(buffer), pFile );
        if( bytesRead > 0 )
        {
            int val = atoi( buffer );
            retVal = val;
        }
        else
        {
            perror( "Error reading from file" );
        }
        fclose (pFile);
    }
    return retVal;
}
