#ifndef SYSFSGPIO_H
#define SYSFSGPIO_H

#include <QObject>
#include <QFile>
#include <QSocketNotifier>


////////////////////////////////////////////////////////////////////////
/// \brief The SysFsGpio class allows control of GPIO's in the linux sys-fs
///
/// This class can be used for interrupt based reading GPI's that are available via sys-fs
/// It uses the virtual files that are avalable at '/sys/class/gpio'.
/// \note the Init memberfunction must be called before the gpio can be used!
class SysFsGpio : public QObject
{
    Q_OBJECT
public:
    explicit SysFsGpio( int gpioNo , QObject *parent = 0 );
    virtual ~SysFsGpio();

    ////////////////////////////////////////////////////////////////////
    /// \brief inititialize gpio. <b>Must be called before gpio can be used</b>
    /// \return 0 on success or -1 on error
    int init();


    ////////////////////////////////////////////////////////////////////
    /// \brief getGpio
    /// \return -1 on error, 0 if gpio-pin is low, 1 if gpio-pin is high
    int getGpio();

signals:
    ////////////////////////////////////////////////////////////////////
    /// \brief gpioChanged signal will be emitted when an interrupt has occured
    /// \param value  gpio value that was read (normally 0 or 1)
    void gpioChanged( int value );

public slots:
    void onGpioInt( int );

protected:
    int             m_gpioNo;
    QFile           m_gpioFile;
    QSocketNotifier *m_gpioNotifier;
    int             m_gpioValue;
};

#endif // SYSFSGPIO_H
