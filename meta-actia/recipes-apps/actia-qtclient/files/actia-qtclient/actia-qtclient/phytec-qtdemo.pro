TEMPLATE = app

QT += qml quick network core

SOURCES += main.cpp \
    client.cpp \
    FileIo.cpp \
    ProcessIf.cpp \
    SysFsGpio.cpp \
    SysFsLed.cpp

RESOURCES += resources.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    client.h \
    namedefs.h \
    FileIo.h \
    ProcessIf.h \
    SysFsGpio.h \
    SysFsLed.h

DISTFILES += \
    qtquickcontrols2.conf \
    content/roboto-regular.ttf \
    qtquickcontrols2.conf

#OTHER_FILES += \
#    ../../../../../../../../../Pictures/Screenshot from 2019-07-27 13:01:14.png

PKG_CONFIG_SYSROOT_DIR = /home/maher/work_space_pfe/repo_phytec/build/tmp/work/cortexa9hf-neon-phytec-linux-gnueabi/iotivity/2.0.0-r1/image

INCLUDEPATH +=\
    $(PKG_CONFIG_SYSROOT_DIR)/usr/include/iotivity/resource \
    $(PKG_CONFIG_SYSROOT_DIR)/usr/include/iotivity/c_common \
    $(PKG_CONFIG_SYSROOT_DIR)/usr/include/iotivity/resource/stack \
    $(PKG_CONFIG_SYSROOT_DIR)/usr/include/iotivity/resource/ocrandom \
    $(PKG_CONFIG_SYSROOT_DIR)/usr/include/iotivity/resource/logger \
    $(PKG_CONFIG_SYSROOT_DIR)/usr/include  \
    $(PKG_CONFIG_SYSROOT_DIR)/usr/include/iotivity/resource/oc_logger
LIBS +=\
        -loc \
        -loctbstack \
        -loc_logger \
        -lmraa \
        -lpthread \

