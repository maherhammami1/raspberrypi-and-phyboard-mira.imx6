//******************************************************************
//
// Copyright 2014 Intel Corporation.
//
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

#ifndef CLIENT_H_
#define CLIENT_H_

#include <QObject>
#include <string>
#include <iostream>
#include <memory>
#include <QObject>
#include <QString>
#include "ocstack.h"
#include "OCApi.h"
#include "OCPlatform.h"
#include "OCResource.h"

using namespace std;
using namespace OC;

class LED : public QObject
{
    Q_OBJECT

    shared_ptr<OCResource> m_resourceHandle;
    OCRepresentation m_ledRepresentation;
    GetCallback m_GETCallback;
    PutCallback m_PUTCallback;
    void onGet(const HeaderOptions&, const OCRepresentation&, int);
    void onPut(const HeaderOptions&, const OCRepresentation&, int);
public:
    explicit LED(QObject *parent = 0);
    Q_INVOKABLE void get();
    Q_INVOKABLE void put(int);
    Q_INVOKABLE LED(shared_ptr<OCResource> Resource);
    Q_INVOKABLE virtual ~LED();
};
class LEDNF : public QObject
{
    Q_OBJECT

    shared_ptr<OCResource> m_resourceHandle;
    OCRepresentation m_lednfRepresentation;
    GetCallback m_GETCallback;
    PutCallback m_PUTCallback;
    void onGet(const HeaderOptions&, const OCRepresentation&, int);
    void onPut(const HeaderOptions&, const OCRepresentation&, int);
public:
    explicit LEDNF(QObject *parent = 0);
    Q_INVOKABLE void get();
    Q_INVOKABLE void put(int);
    Q_INVOKABLE LEDNF(shared_ptr<OCResource> Resource);
    Q_INVOKABLE virtual ~LEDNF();
};
class LEDD : public QObject
{
    Q_OBJECT

    shared_ptr<OCResource> m_resourceHandle;
    OCRepresentation m_leddRepresentation;
    GetCallback m_GETCallback;
    PutCallback m_PUTCallback;
    void onGet(const HeaderOptions&, const OCRepresentation&, int);
    void onPut(const HeaderOptions&, const OCRepresentation&, int);
public:
    explicit LEDD(QObject *parent = 0);
    Q_INVOKABLE void get();
    Q_INVOKABLE void put(int);
    Q_INVOKABLE LEDD(shared_ptr<OCResource> Resource);
    Q_INVOKABLE virtual ~LEDD();
};
class LEDG : public QObject
{
    Q_OBJECT

    shared_ptr<OCResource> m_resourceHandle;
    OCRepresentation m_ledgRepresentation;
    GetCallback m_GETCallback;
    PutCallback m_PUTCallback;
    void onGet(const HeaderOptions&, const OCRepresentation&, int);
    void onPut(const HeaderOptions&, const OCRepresentation&, int);
public:
    explicit LEDG(QObject *parent = 0);
    Q_INVOKABLE void get();
    Q_INVOKABLE void put(int);
    Q_INVOKABLE LEDG(shared_ptr<OCResource> Resource);
    Q_INVOKABLE virtual ~LEDG();
};
class LEDGD : public QObject
{
    Q_OBJECT

    shared_ptr<OCResource> m_resourceHandle;
    OCRepresentation m_ledgdRepresentation;
    GetCallback m_GETCallback;
    PutCallback m_PUTCallback;
    void onGet(const HeaderOptions&, const OCRepresentation&, int);
    void onPut(const HeaderOptions&, const OCRepresentation&, int);
public:
    explicit LEDGD(QObject *parent = 0);
    Q_INVOKABLE void put(int);
    Q_INVOKABLE void get();
    Q_INVOKABLE LEDGD(shared_ptr<OCResource> Resource);
    Q_INVOKABLE virtual ~LEDGD();
};

class TemperatureSensor : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString value READ value WRITE setvalue NOTIFY valueChanged)
    QString m_value;
    shared_ptr<OCResource> m_resourceHandle;
    OCRepresentation m_temperatureRepresentation;
    GetCallback m_GETCallback;
    ObserveCallback m_OBSERVECallback;
    bool m_isObserved;
    void onObserve(const HeaderOptions headerOptions, const OCRepresentation& rep, int eCode, int sequenceNumber);
    void onGet(const HeaderOptions&, const OCRepresentation&, int);
public:
    explicit TemperatureSensor(QObject *parent = 0);
    void get();
    QString value()
    {
        return m_value;
    }
    void setvalue(QString &val)
    {
        if(m_value != val)
        {
            m_value = val;
            emit valueChanged();
        }
    }
    void startObserve();
    void stopObserve();
    TemperatureSensor(shared_ptr<OCResource> Resource);
    virtual ~TemperatureSensor();
signals:
    void valueChanged();

};
class IoTClient : public QObject
{
    Q_OBJECT
    shared_ptr<TemperatureSensor> m_temperatureSensor;
    shared_ptr<LED> m_platformLED;
    shared_ptr<LEDNF> m_platformLEDNF;
    shared_ptr<LEDD> m_platformLEDD;
    shared_ptr<LEDG> m_platformLEDG;
    shared_ptr<LEDGD> m_platformLEDGD;
    shared_ptr<PlatformConfig> m_platformConfig;
    FindCallback m_resourceDiscoveryCallback;
    void initializePlatform();
    void discoveredResource(shared_ptr<OCResource>);
public:
    shared_ptr<TemperatureSensor> getTemperatureSensor();
    Q_INVOKABLE shared_ptr<LED> getPlatformLED();
    Q_INVOKABLE shared_ptr<LEDNF> getPlatformLEDNF();
    Q_INVOKABLE shared_ptr<LEDD> getPlatformLEDD();
    Q_INVOKABLE shared_ptr<LEDG> getPlatformLEDG();
    Q_INVOKABLE shared_ptr<LEDGD> getPlatformLEDGD();
    Q_INVOKABLE void findResource();
    Q_INVOKABLE void gettemperature();
    Q_INVOKABLE void putledonfar();
    Q_INVOKABLE void putledon();
    Q_INVOKABLE void putledoff();
    Q_INVOKABLE void putledclinodroit();
    Q_INVOKABLE void putfourclino();
    Q_INVOKABLE void putledclinogauche();
    IoTClient();
    virtual ~IoTClient();
    static void DisplayMenu();
};

#endif /* CLIENT_H_ */
