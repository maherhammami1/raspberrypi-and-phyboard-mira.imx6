#ifndef SYSFSLED_H
#define SYSFSLED_H


/////////////////////////////////////////////////////////////////////
/// \brief The SysFsLed class allows Qt-independent access to user LED's
///
/// \note This class is not reentrant or threadsave!
///
class SysFsLed
{
public:
    SysFsLed( const char* sysFsFile );
    ~SysFsLed();

    ////////////////////////////////////////////////////////////////
    /// \brief setLed
    /// \param value brightness value. Must be between 0 and 255
    /// \return -1 on error or 0 on success
    int setLed( int value );

    ////////////////////////////////////////////////////////////////
    /// \brief getLed
    /// \return the current value of the led read from sysfs
    int getLed();

private:
    char* m_sysFsFile;
};

#endif // SYSFSLED_H
