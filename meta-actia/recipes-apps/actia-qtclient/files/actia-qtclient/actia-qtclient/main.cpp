#include <QtGui/QGuiApplication>
#include <QQmlApplicationEngine>
#include <QCoreApplication>
#include <QFont>
#include <QDebug>
#include <QtQuick>

#include "client.h"
#include "namedefs.h"
#include "FileIo.h"
#include "SysFsGpio.h"
#include "SysFsLed.h"
#include "ProcessIf.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    IoTClient client;
    client.findResource();
    TemperatureSensor*tmp;
    qmlRegisterType<IoTClient>("IoTClient",1 ,0, "IoTClient");
//    qmlRegisterType<TemperatureSensor>("TemperatureSensor",1 ,0, "TemperatureSensor");
    engine.rootContext()->setContextProperty("temperature",tmp);
    qmlRegisterType<LED>("LED",1 ,0, "LED");
    qmlRegisterType<LEDNF>("LEDNF",1 ,0, "LEDNF");
    qmlRegisterType<LEDD>("LEDD",1 ,0, "LEDD");
    qmlRegisterType<LEDG>("LEDG",1 ,0, "LEDG");
    qmlRegisterType<LEDGD>("LEDGD",1 ,0, "LEDGD");
    qmlRegisterSingletonType<ProcessIf>("FriWare.ProcessIf", 1, 0, "ProcessIf", ProcessIf::processif_singletontype_provider );
    qmlRegisterType<FileIO>("FriWare.FileIO", 1, 0, "FileIO");
    int id = QFontDatabase::addApplicationFont(":/fonts/roboto-regular.ttf");
    if (id == -1) {
        qDebug("Cannot open font roboto-regular");
    } else {
        QString family = QFontDatabase::applicationFontFamilies(id).at(0);
        QFont roboto(family);
        app.setFont(roboto);
    }

    id = QFontDatabase::addApplicationFont(":/fonts/icomoon.ttf");
    if (id == -1)
        qDebug("Cannot open font icomoon");
    id = QFontDatabase::addApplicationFont(":/fonts/fontawesome-webfont.ttf");
    if (id == -1)
        qDebug("Cannot open font fontawesome");
    app.setOrganizationName( "Phytec" );
    app.setApplicationName( "PhyKitDemo" );
    QSettings settings;
    QFile file(settings.fileName());
    if( !file.exists() )
    {
        qDebug( "WARNING: Settings file %s not found!\n", settings.fileName().toLatin1().data() );
        qDebug( "Writing new settings file with default values.\n" );
    }

    QVariantList pages;
    int size = settings.beginReadArray("enabled_pages");
    for (int i = 0; i < size; ++i) {
        settings.setArrayIndex(i);
        pages.append(settings.value("name").toString());
    }
    settings.endArray();
    engine.rootContext()->setContextProperty("enabledPages", pages);
    engine.rootContext()->setContextProperty("doExpensiveDrawOperations", settings.value("doExpensiveDrawOperations").toBool());
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    return app.exec();
}
