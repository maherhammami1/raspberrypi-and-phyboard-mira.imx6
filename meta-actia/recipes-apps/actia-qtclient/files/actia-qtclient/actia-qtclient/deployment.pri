linux-oe-g++ {
    message( embedded target-deployment )

    deployment.path = $$(datadir)/phytec-qtdemo
    target.path = $$(datadir)/phytec-qtdemo

    INSTALLS += target \
                deployment

    export(INSTALLS)
}
