import QtQuick 2.3
import QtQuick.Controls 2.0
import Qt.labs.folderlistmodel 2.1
import FriWare.ProcessIf 1.0
import FriWare.FileIO 1.0

Rectangle {
    id: root
    width: parent.width * 0.7
    height: parent.height * 0.7
    color: "lightgrey"
    state: "NOTVISIBLE"
    focus: true
    clip: true

    property string selectedFilePath : ""
    property string selectedFileURL : ""
    property string selectedFileName : ""
    property alias folder : folder_model.folder
    property alias nameFilters : folder_model.nameFilters

    signal okClicked( string selectedFilePath )
    signal cancelClicked

    FileIO{
        id: fileIO
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Functions

    function open() {
        state = "VISIBLE"
    }

    // Evaluates the current selection in the view.
    // Called by onDoubleClicked from delgate or when OH-button is clicked
    function evalSelection( selected_index )
    {
        /*
        console.log( "baseName: " + folder_model.get(selected_index, "fileBaseName" ) )
        console.log( "fileName: " + folder_model.get(selected_index, "fileName" ) )
        console.log( "folder: " + folder_model.folder )
        */

        // Check if current directory exists
        if( fileIO.dirExists( folder_model.folder) ) {
            // console.log( "dir: " + folder_model.folder + " exists" )
        }
        else {
            console.warn( "WARNING: Directory  '" + folder_model.folder + "' does NOT exists! Setting to 'file:///'" )
            folder_model.folder = "file:///"
            return
        }

        if( folder_model.isFolder(selected_index) ) {
            // a folder was selected
            if( folder_model.get( selected_index, "fileName") == ".." ) {
                if( folder_model.folder == "file:///") {
                    // we are in root folder, so ignore
                }
                else {
                    folder_model.folder = folder_model.parentFolder
                }
            }
            else if( folder_model.get( selected_index, "fileName") == "." ) {
                // ignore single dot (means current dir)
            }
            else {
                if( folder_model.folder == "file:///") {
                    folder_model.folder = folder_model.folder  + folder_model.get( selected_index, "fileName" )
                }
                else {
                    console.log( "new folder: " + folder_model.folder  + "/" + folder_model.get( selected_index, "fileName" ) )
                    folder_model.folder = folder_model.folder  + "/" + folder_model.get( selected_index, "fileName" )
                }
            }
        }
        else {
            // handle file selection here
            // console.log( folder_model.get(selected_index, "filePath" ) )

            root.selectedFilePath = folder_model.get(selected_index, "filePath" )
            root.selectedFileURL = folder_model.get(selected_index, "fileURL" )
            root.selectedFileName = folder_model.get(selected_index, "fileName" )
            okClicked( root.selectedFilePath )
            root.state = "NOTVISIBLE"
        }
    }

    states: [
        State {
            name: "VISIBLE"
            PropertyChanges {
                target: root; visible: true
            }
        },
        State {
            name: "NOTVISIBLE"
            PropertyChanges {
                target: root; visible: false
            }
        }
    ]


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Model data and behaviour
    FolderListModel {
        id: folder_model
        showDotAndDotDot: true
        showDirsFirst: true
        //nameFilters: [ "*.mp4", "*.mpg" ]

        onFolderChanged: {
            folder_name_txt.text = folder_model.folder
        }

        Component.onCompleted: {
            //folder_model.folder = "file:///home/Videos"
            folder_name_txt.text = folder_model.folder
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Delegate
    Component {
        id: file_delegate
        Item {
            width: root.width
            height: 32
            Text {
                font.family: "FontAwesome"
                font.pixelSize: parent.height * 0.5
                text: folder_model.isFolder(index) ? icons.fa_folder_o : icons.fa_file_o
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 5
            }

            Text {
                id: txt_fileName
                width: root.width * 0.66
                x: 36
                anchors.verticalCenter: parent.verticalCenter
                text: fileName
                color: "black"
                clip: true
            }
            Text {
                width: root.width * 0.33
                x: txt_fileName.width + txt_fileName.x
                anchors.verticalCenter: parent.verticalCenter
                text: (fileSize/1024).toFixed(2) + " kB"
                color: "black"
                clip: true
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log( "Clicked " + fileName )
                    console.log( "Current folder: " + folder_model.folder )
                    list_view.currentIndex = index
                }
                onDoubleClicked: {
                    evalSelection( index )
                }
            }
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Here the current folder is displayed
    Rectangle {
        id: folder_name_rect
        width: root.width
        height: 32
        color: "darkgray"
        Text {
            id: folder_name_txt
            anchors.left: parent.left
            anchors.leftMargin: 8
            anchors.verticalCenter: parent.verticalCenter
            text: folder_model.folder
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Listview definition
    ListView {
        id: list_view
        width: root.width
        height: 480 - folder_name_rect.height - 120
        y: folder_name_rect.y + folder_name_rect.height
        clip: true

        model: folder_model
        delegate: file_delegate
        highlight: Rectangle {
            width: list_view.width
            height: 16
            color: "#303040"
        }
    }

    /////////////////////////////////////////////////////////////////////////
    // Buttons OK and Cancel
    Row {
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 24
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 8
        Rectangle {
            id: btn_ok
            width: 84; height: 48
            color: "darkgrey"
            radius: 5
            Text {
                anchors.centerIn: parent
                text: "Ok"
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    evalSelection( list_view.currentIndex )
                }
            }
        }
        Rectangle {
            id: bnt_cancel
            width: 84; height: 48
            color: "darkgrey"
            radius: 5
            Text {
                anchors.centerIn: parent
                text: "Cancel"
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    root.state = "NOTVISIBLE"
                    cancelClicked() //emit the signal
                }
            }
        }
    }
}
