import QtQuick 2.0
import QtQuick.Controls 2.0

Rectangle {
    id: dialog
    anchors.centerIn: parent
    width: root.width * 0.3
    height: root.height * 0.3
    border.width: 3
    border.color: "darkgrey"
    radius: width*0.05
    color: "lightgrey"
    state: "hide"
    focus: true
    //clip: true

    property string iconWarning: icons.fa_warning

    property int typeOk: 0
    property int typeOkCancel: 1

    property string title : ""
    property string text : ""
    property string icon : ""
    property int type: -1

    onTypeChanged: {
        switch(type) {
            case typeOk:
                btn_ok.visible = true;
                break;
            case typeOkCancel:
                btn_ok.visible = true;
                btn_cancel.visible = true;
                break;
        }
    }

    signal okClicked
    signal cancelClicked

    function open() {
        state = "show"
    }

    states: [
            State {
                name: "show"; PropertyChanges { target: dialog; visible: true; opacity: 1.0  }
            },
            State {
                name: "hide"; PropertyChanges { target: dialog; opacity: 0.0; visible: false  }
            }
    ]
    transitions: [
        Transition {
            from: "hide"; to: "show";
            SequentialAnimation{
                NumberAnimation { properties: "visible"; duration: 0 }
                NumberAnimation { properties: "opacity"; duration: 300 }
            }
        },
        Transition {
            from: "show"; to: "hide";
            SequentialAnimation {
                NumberAnimation { properties: "opacity"; duration: 300 }
                NumberAnimation { properties: "visible"; duration: 0 }
            }
        }
    ]

    Rectangle {
        id: titleBar
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: dialog.top
        anchors.topMargin: 10
        height: dialog.height / 5
        width: dialog.width - 25
        color: "darkgrey"
        radius: dialog.radius
        Text {
            id: dialogTitle
            anchors.centerIn: parent
            font.pixelSize: parent.height * 0.5
            text: dialog.title
            color: "white"
        }
    }

    Text {
        id: dialogIcon
        anchors.top: titleBar.bottom
        anchors.topMargin: parent.height / 12
        anchors.left: parent.left
        anchors.leftMargin: parent.width / 12

        font.family: "FontAwesome"
        font.pixelSize: parent.height / 5
        text: dialog.icon
    }

    Text {
        id: dialogText
        anchors.top: dialogIcon.top
        anchors.left: dialogIcon.right
        anchors.leftMargin: parent.width / 12
        anchors.right: parent.right
        anchors.rightMargin: parent.width / 12
        font.pixelSize: (parent.height / 5) * 0.5
        height: parent.height * 3 / 5
        text: dialog.text
        wrapMode: Text.WordWrap
        color: "black"
    }

    Row {
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottomMargin: 10
        spacing: 10

        Button {
            id: btn_ok
            height: dialog.height / 5 - 10
            width: height * 2
            text: "Ok"
            font.pixelSize: (dialog.height / 5) * 0.3
            visible: false
            onPressed: {
                okClicked()
                dialog.state = "hide"
            }
            background: Rectangle {
                color: btn_ok.down ? "#d6d6d6" : "#f6f6f6"
                border.color: "#26282a"
                border.width: 1
                radius: 5
            }
        }
        Button {
            id: btn_cancel
            height:dialog.height / 5 - 10
            width: height * 2
            text: "Cancel"
            font.pixelSize: (dialog.height / 5) * 0.3
            visible: false
            onPressed: {
                cancelClicked()
                dialog.state = "hide"
            }
            background: Rectangle {
                color: btn_cancel.down ? "#d6d6d6" : "#f6f6f6"
                border.color: "#26282a"
                border.width: 1
                radius: 5
            }
        }
    }
}
