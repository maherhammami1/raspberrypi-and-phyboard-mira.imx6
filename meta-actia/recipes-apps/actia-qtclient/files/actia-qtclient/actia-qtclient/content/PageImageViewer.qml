import QtQuick 2.3
import Qt.labs.folderlistmodel 2.1
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.2
import QtQuick 2.4
import QtQuick.Controls 2.0
import QtQuick 2.3
import QtQml.Models 2.2
import QtQuick.Window 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.2
import QtQuick 2.0
import QtQuick 2.2
import QtQuick.Layouts 1.2
import IoTClient 1.0
import LED 1.0
import LEDNF 1.0
import LEDD 1.0
import LEDG 1.0
import LEDGD 1.0
ColumnLayout {
    id: layout
   // property alias mouseArea: mouseArea
    anchors.fill: parent
    spacing: 0

    ListModel {
        id: pagemenuviewer
        ListElement {
            name: "Image Viewer"
            icon: "\uf03e" //icons.fa_picture_o
            pageSource: "PageMenuViewer.qml"
        }
    }
    ListModel {
        id: pagereglageviewer
        ListElement {
            name: "Image Viewer"
            icon: "\uf03e" //icons.fa_picture_o
            pageSource: "reglage.qml"
        }
    }
    ListModel {
        id: pageinfoviewer
        ListElement {
            name: "Image Viewer"
            icon: "\uf03e" //icons.fa_picture_o
            pageSource: "info.qml"
        }
    }
    function selectItemHome() {
        var myModelItem = pagemenuviewer.get(configuredPages.model_index_map[view.currentIndex])
        client_area.clientUrl = myModelItem.pageSource
        client_area.title = myModelItem.name
        mainqml.color = "white"
        view.visible = false
      }
    function selectItemReglage() {
        var myModelItem = pagereglageviewer.get(configuredPages.model_index_map[view.currentIndex])
        client_area.clientUrl = myModelItem.pageSource
        client_area.title = myModelItem.name
        mainqml.color = "white"
        view.visible = false
      }
    function selectItemInfo() {
        var myModelItem = pageinfoviewer.get(configuredPages.model_index_map[view.currentIndex])
        client_area.clientUrl = myModelItem.pageSource
        client_area.title = myModelItem.name
        mainqml.color = "white"
        view.visible = false
      }

    IoTClient{
        id:client
    }
    LED{
        id:led
    }
    LEDNF{
        id:lednf
    }
    LEDD{
        id:ledd
    }
    LEDG{
        id:ledg
    }
    LEDGD{
        id:ledgd
    }

    Image {
        id: main
        source: "qrc:/images/1.jpeg"
        Layout.fillWidth: true
        Layout.fillHeight: true
        MouseArea{
            anchors.fill: parent
            onClicked:
            {
                powermenu.visible = false
                reg.visible = false
            }
        }

        property real tempeature: 0

        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 355; leftMargin: 355; bottomMargin: 260
            }
            height: 75
            id: clino
            source: "qrc:/images/4clino.png"

        MouseArea{
            anchors.fill: parent
            onClicked:{
                client.putfourclino()
             }
        }
   }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 30; leftMargin: 735; bottomMargin: 20
            }
            height: 50
            id: name4
            source: "qrc:/images/fleched.png"

        MouseArea{
            anchors.fill: parent
            onClicked:{
                client.putledclinodroit()
             }
        }
   }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 735; leftMargin: 30; bottomMargin: 20
            }
            height: 50
            id: name5
            source: "qrc:/images/flechg.png"

        MouseArea{
            anchors.fill: parent
            onClicked:{
                client.putledclinogauche()
             }
        }
   }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 470; leftMargin: 305; bottomMargin: 20
            }
            height: 30
            id: indicatorslights1
            source: "qrc:/images/IndicatorsLights1.png"

        MouseArea{
            anchors.fill: parent
            onClicked:{
                client.putledonfar()
             }
        }
   }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 430; leftMargin: 345; bottomMargin: 20
            }
            height: 30
            id: indicatorslights2
            source: "qrc:/images/IndicatorsLights2.png"

        MouseArea{
            anchors.fill: parent
            onClicked:{
                client.putledon()
             }
        }
   }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 390; leftMargin: 385; bottomMargin: 20
            }
            height: 30
            id: indicatorslights3
            source: "qrc:/images/IndicatorsLights3.png"

        MouseArea{
            anchors.fill: parent
            onClicked:{
                client.putledoff()
             }
        }
   }

        Text {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 390; leftMargin: 370; bottomMargin: 230
            }
            id: timeText
            text : Qt.formatTime(currentDate, "hh:mm")
            color: "white"
            property var currentDate: new Date();

            Timer {
                interval: 1000
                repeat: true
                running: true
                onTriggered: {
                    timeText.currentDate = new Date();
                }
            }
        }
        Text {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 385; leftMargin: 380; bottomMargin: 80
            }
            id: temperaturetext
            text: main.tempeature + "°C"
            color: "white"
        }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 350; leftMargin: 425; bottomMargin: 20
            }
            height: 30
            id: indicatorslights4
            source: "qrc:/images/IndicatorsLights4.png"

        MouseArea{
            anchors.fill: parent
            onClicked:{
             }
        }
   }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 310; leftMargin: 465; bottomMargin: 20
            }
            height: 30
            id: indicatorslights5
            source: "qrc:/images/IndicatorsLights5.png"

        MouseArea{
            anchors.fill: parent
            onClicked:{
             }
        }
   }

        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 735; leftMargin: 10; bottomMargin: parent.height -60
            }
            height: 50
            id: tp
            source: "qrc:/images/SelectMenu.png"

        MouseArea{
            anchors.fill: parent
            onClicked:{
                powermenu.visible = true
                reg.visible = true
             }
        }
   }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 735; leftMargin: 10; bottomMargin: parent.height - 130
            }
            height: 50
            id: powermenu
            source: "qrc:/images/InfoLogo.png"
            visible: false

        MouseArea{
            anchors.fill: parent
            onClicked:{
                selectItemInfo()
             }
        }
   }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 735; leftMargin: 10; bottomMargin: parent.height - 200
            }
            height: 50
            id: reg
            source: "qrc:/images/Regg.png"
            visible: false

        MouseArea{
            anchors.fill: parent
            onClicked:{
                selectItemReglage()
             }
        }
   }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 10; leftMargin: 740; bottomMargin: parent.height - 60
            }
            height: 50
            id: powermenu1
            source: "qrc:/images/homeLogo.png"

        MouseArea{
            anchors.fill: parent
            onClicked: selectItemHome()
        }
   }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 430; leftMargin: 70; bottomMargin: parent.height -450
            }
            height: 280
            id: gauge
            source: "qrc:/images/Gauges2.png"
            property real value //: client.gettemperature()
            property bool metricSystem: false
            Image {
               anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                    rightMargin: 120; leftMargin: 120; bottomMargin: parent.height -160
                }
                height: 50
                id: centre2
                source: "qrc:/images/20.png"
       }
            Text {
                id: speedText
                anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                     rightMargin: 110; leftMargin: 140;bottomMargin: parent.height -200
                 }
                font.pixelSize: 30
                font.letterSpacing: 4
                color: "white"
                text: gauge.value
            }
            Image {
               anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                    rightMargin: 110; leftMargin: 140; bottomMargin: parent.height -255
                }
                height: 35
                id: indicatorsfuel
                source: "qrc:/images/IndicatorsFuel.png"

       }
            //! [needle]
                Image {
                    id: needle12
                    x: 140; y: 70
                    antialiasing: true
                    source: "qrc:/images/needle.png"
                    transform: Rotation {
                        id: needleRotation12
                        origin.x: 9; origin.y: 65
                        //! [needle angle]
                        angle: Math.min(Math.max(-130, gauge.value*2.6 - 130), 133)
                        Behavior on angle {
                            SpringAnimation {
                                spring: 1.4
                                damping: .15
                            }
                        }
                        //! [needle angle]
                    }
                }
            //! [needle]
   }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 70; leftMargin: 430; bottomMargin: parent.height -450
            }
            height: 280
            id: gauge22
            source: "qrc:/images/Gauges22.png"

            property real value : 0.0
            property bool metricSystem: false

            Image {
               anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                    rightMargin: 120; leftMargin: 120; bottomMargin: parent.height -160
                }
                height: 50
                id: centre
                source: "qrc:/images/20.png"

       }
            Image {
               anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                    rightMargin: 110; leftMargin: 110; bottomMargin: parent.height -275
                }
                height: 70
                id: huile
                source: "qrc:/images/huile.png"

       }
            Text {
                id: powerText
                anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                     rightMargin: 110; leftMargin: 140;bottomMargin: parent.height -200
                 }
                font.pixelSize: 30
                font.letterSpacing: 4
                color: "white"
                text: gauge22.value
            }
            //! [needle]
                Image {
                    id: needle1
                    x: 140; y: 70
                    antialiasing: true
                    source: "qrc:/images/needle.png"
                    transform: Rotation {
                        id: needleRotation1
                        origin.x: 9; origin.y: 65
                        //! [needle angle]
                        angle: Math.min(Math.max(-130, gauge22.value*26 - 130), 133)
                        Behavior on angle {
                            SpringAnimation {
                                spring: 1.4
                                damping: .15
                            }
                        }
                        //! [needle angle]
                    }
                }
            //! [needle]
   }
        /*Item {
            id: root1
            property real value

            width: 210; height: 210

            //anchors.centerIn: parent
            y:73
            x:250
            value:  119.23  // slider1.x * 100 / (container1.width - 32)


        //! [needle_shadow]
            Image {
                x: 135
                y: 80
                source: "qrc:/images/needle_shadow.png"
                transform: Rotation {
                    origin.x: 9; origin.y: 67
                    angle: needleRotation1.angle
                }
            }
        //! [needle_shadow]
        //! [needle]
            Image {
                id: needle1
                x: 36; y: 112
                antialiasing: true
                source: "IconFonts.qml"
                transform: Rotation {
                    id: needleRotation1
                    origin.x: 5; origin.y: 65
                    //! [needle angle]
                    angle: Math.min(Math.max(-130, root1.value*2.6 - 130), 133)
                    Behavior on angle {
                        SpringAnimation {
                            spring: 1.4
                            damping: .15
                        }
                    }
                    //! [needle angle]
                }
            }
        //! [needle]
        }*/
        /*Rectangle {
            id: container1
            property int oldWidth: 0
            anchors { bottom: parent.bottom; left: parent.left
                right: parent.right; leftMargin: 80; rightMargin: 80
                bottomMargin: 50
            }
            height: 20
            radius: 8
            opacity: 0.7
            antialiasing: true
            visible: true
            gradient: Gradient {
                GradientStop { position: 0.0; color: "gray" }
                GradientStop { position: 1.0; color: "white" }
            }
            readonly property int sliderSize: 38
            onWidthChanged: {
                if (oldWidth === 0) {
                    //items in layouts get the size during the
                    //second resize, the first resize has negative
                    //width values
                    if (width < 0)
                        return
                    oldWidth = width;
                    return
                }
                var desiredPercent = (slider1.x) * 100 / (oldWidth - sliderSize)
                slider1.x = (desiredPercent * (width - sliderSize) / 100)
                oldWidth = width
            }
            Rectangle {
                id: slider1
                x:1
                y: 1; width: parent.sliderSize; height: 18
                radius: 6
                visible: true
                antialiasing: true
                gradient: Gradient {
                    GradientStop { position: 0.0; color: "#424242" }
                    GradientStop { position: 1.0; color: "black" }
                }
                MouseArea {
                    anchors.fill: parent
                    anchors.margins: -16 // Increase mouse area a lot outside the slider
                    drag.target: slider1; drag.axis: Drag.XAxis
                    drag.minimumX: 2; drag.maximumX: container1.width - container1.sliderSize - 2
                }
            }
        }*/
    }
}

