import QtQuick 2.3
import Qt.labs.folderlistmodel 2.1
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.2
import QtQuick 2.4
import QtQuick.Controls 2.0
import QtQuick 2.3
import QtQml.Models 2.2
import QtQuick.Window 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.2
import QtQuick 2.0
import QtQuick 2.2
import QtQuick.Layouts 1.2
ColumnLayout {
    id: layout
   // property alias mouseArea: mouseArea
    anchors.fill: parent
    spacing: 0

    ListModel {
        id: pageclusterviewer
        ListElement {
            name: "Image Viewer"
            icon: "\uf03e" //icons.fa_picture_o
            pageSource: "PageImageViewer.qml"
        }
    }
    ListModel {
        id: pagemapsviewer
        ListElement {
            name: "Image Viewer"
            icon: "\uf03e" //icons.fa_picture_o
            pageSource: "PageGpsViewer.qml"
        }
    }
    ListModel {
        id: pagephoneviewer
        ListElement {
            name: "Image Viewer"
            icon: "\uf03e" //icons.fa_picture_o
            pageSource: "PagePhoneViewer.qml"
        }
    }
    ListModel {
        id: pagemusicviewer
        ListElement {
            name: "Image Viewer"
            icon: "\uf03e" //icons.fa_picture_o
            pageSource: "PageMusicViewer.qml"
        }
    }
    ListModel {
        id: pageradioviewer
        ListElement {
            name: "Image Viewer"
            icon: "\uf03e" //icons.fa_picture_o
            pageSource: "PageRadio.qml"
        }
    }
    ListModel {
        id: pagevideoviewer
        ListElement {
            name: "Image Viewer"
            icon: "\uf03e" //icons.fa_picture_o
            pageSource: "PageVideo.qml"
        }
    }
    ListModel {
        id: pageblueviewer
        ListElement {
            name: "Image Viewer"
            icon: "\uf03e" //icons.fa_picture_o
            pageSource: "PageBluetooth.qml"
        }
    }
    Image {
        id: main
        source: "qrc:/images/1.jpeg"
        Layout.fillWidth: true
        Layout.fillHeight: true

        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 230; leftMargin: 440; bottomMargin: (parent.height /2)
            }
            height: 120
            id: localisation
            source: "qrc:/images/google_maps.png"
            MouseArea{
                anchors.fill: parent
                onClicked: selectItemMaps()
            }
   }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 570; leftMargin: 40; bottomMargin: (parent.height /2)
            }
            height: 120
            id: phone
            source: "qrc:/images/phone.png"
            MouseArea{
                anchors.fill: parent
                onClicked: selectItemPhone()
            }
   }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 50; leftMargin: 610; bottomMargin: (parent.height /2)
            }
            height: 120
            id: home
            source: "qrc:/images/home2.png"
            MouseArea{
                anchors.fill: parent
                onClicked: selectItemCluster()
            }
   }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 400; leftMargin: 250; bottomMargin: (parent.height /2) -5
            }
            height: 135
            id: music
            source: "qrc:/images/music.png"
            MouseArea{
                anchors.fill: parent
                onClicked: selectItemMusic()
            }

    }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 230; leftMargin: 440; bottomMargin: 80
            }
            height: 120
            id: bluetooth
            source: "qrc:/images/bluetooth.png"
            MouseArea{
                anchors.fill: parent
                onClicked: selectItemBluetooth()
            }
   }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 585; leftMargin: 60; bottomMargin: 80
            }
            height: 120
            id: setting
            source: "qrc:/images/parametre.png"
            MouseArea{
                anchors.fill: parent
                onClicked: selectItemPhone()
            }
   }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 40; leftMargin: 600; bottomMargin: 80
            }
            height: 145
            id: video
            source: "qrc:/images/video.png"
            MouseArea{
                anchors.fill: parent
                onClicked: selectItemVideo()
            }
   }
        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 400; leftMargin: 250; bottomMargin: 50
            }
            height: 190
            id: radio
            source: "qrc:/images/pngegg.png"
            MouseArea{
                anchors.fill: parent
                onClicked: selectItemRadio()
            }

    }
}
    function selectItemCluster() {
        var myModelItem = pageclusterviewer.get(configuredPages.model_index_map[view.currentIndex])
        client_area.clientUrl = myModelItem.pageSource
        client_area.title = myModelItem.name
        mainqml.color = "white"
        view.visible = false
      }
    function selectItemMusic() {
        var myModelItem = pagemusicviewer.get(configuredPages.model_index_map[view.currentIndex])
        client_area.clientUrl = myModelItem.pageSource
        client_area.title = myModelItem.name
        mainqml.color = "white"
        view.visible = false
      }
    function selectItemPhone() {
        var myModelItem = pagephoneviewer.get(configuredPages.model_index_map[view.currentIndex])
        client_area.clientUrl = myModelItem.pageSource
        client_area.title = myModelItem.name
        mainqml.color = "white"
        view.visible = false
      }
    function selectItemMaps() {
        var myModelItem = pagemapsviewer.get(configuredPages.model_index_map[view.currentIndex])
        client_area.clientUrl = myModelItem.pageSource
        client_area.title = myModelItem.name
        mainqml.color = "white"
        view.visible = false
      }
    function selectItemRadio() {
        var myModelItem = pageradioviewer.get(configuredPages.model_index_map[view.currentIndex])
        client_area.clientUrl = myModelItem.pageSource
        client_area.title = myModelItem.name
        mainqml.color = "white"
        view.visible = false
      }
    function selectItemVideo() {
        var myModelItem = pagevideoviewer.get(configuredPages.model_index_map[view.currentIndex])
        client_area.clientUrl = myModelItem.pageSource
        client_area.title = myModelItem.name
        mainqml.color = "white"
        view.visible = false
      }
    function selectItemBluetooth() {
        var myModelItem = pageblueviewer.get(configuredPages.model_index_map[view.currentIndex])
        client_area.clientUrl = myModelItem.pageSource
        client_area.title = myModelItem.name
        mainqml.color = "white"
        view.visible = false
      }
}


