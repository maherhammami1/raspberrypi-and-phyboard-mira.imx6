import QtQuick 2.3
import QtQml.Models 2.1
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.0

ColumnLayout {
id: layout
anchors.fill: parent
spacing: 0

Menubar {}

Item {
    id: main
    Layout.fillWidth: true
    Layout.fillHeight: true

    ObjectModel {
        id: itemModel
        Image {
            source: "qrc:/images/page_phytec_1.png"
            width: view.width
            height: view.height
            fillMode: Image.PreserveAspectFit
        }

        Image {
            source: "qrc:/images/page_phytec_2.png"
            width: view.width
            height: view.height
            fillMode: Image.PreserveAspectFit
        }

        Image {
            source: "qrc:/images/page_phytec_3.png"
            width: view.width
            height: view.height
            fillMode: Image.PreserveAspectFit
        }
    }

    ListView {
        id: view
        clip: true
        anchors { fill: parent; bottomMargin: 30 }
        model: itemModel
        preferredHighlightBegin: 0; preferredHighlightEnd: 0
        highlightRangeMode: ListView.StrictlyEnforceRange
        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem; flickDeceleration: 2000
        cacheBuffer: 200
        highlightMoveDuration: 500

        RowLayout {
            anchors.fill: parent
            Button {
                id: fwd
                font.pixelSize: parent.height * 0.5
                font.family: "FontAwesome"
                text: icons.fa_angle_left
                flat: true
                z: 99
                opacity: 0.0
                Behavior on opacity {
                       NumberAnimation { duration: 200 }
                }
                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: view.decrementCurrentIndex()
                    onEntered: {fwd.opacity = 0.5}
                    onExited: {fwd.opacity = 0}

                }
            }
            Item  {Layout.fillWidth: true}
            Button {
                id: back
                font.pixelSize: parent.height * 0.5
                font.family: "FontAwesome"
                text: icons.fa_angle_right
                flat: true
                z: 99
                opacity: 0.0
                Behavior on opacity {
                       NumberAnimation { duration: 200 }
                }
                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: view.incrementCurrentIndex()
                    onEntered: {back.opacity = 0.5}
                    onExited: {back.opacity = 0}
                }
            }
        }
    }



    Rectangle {
        width: view.width; height: 30
        anchors { top: view.bottom; bottom: parent.bottom }
        color: "transparent"

        Row {
            anchors.centerIn: parent
            spacing: 20

            Repeater {
                model: itemModel.count

                Rectangle {
                    width: 10; height: 10
                    border.color: "grey"
                    radius: 3
                    color: view.currentIndex == index ? "green" : "white"

                    MouseArea {
                        width: 20; height: 20
                        anchors.centerIn: parent
                        onClicked: {view.currentIndex = index}
                    }
                }
            }
        }
    }
}
}
