import QtQuick 2.0
import QtMultimedia 5.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.2

Item {
ColumnLayout {
id: layout
anchors.fill: parent
spacing: 0

Menubar {}

Item {
    id: main
    Layout.fillWidth: true
    Layout.fillHeight: true

    MediaPlayer {
        id: mediaPlayer
        autoPlay: true
        onError: {
            dialog.title = "Media Player Error"
            dialog.text = errorString
            dialog.icon = dialog.iconWarning
            dialog.type = dialog.typeOk
            dialog.open()
        }

    }

    VideoOutput {
        id: video
        anchors.fill: parent
        source: mediaPlayer
    }

    // File selector
    Text {
        id: txt_fileinfo
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 16
        font.bold: true
        color: "grey"
        text: mediaPlayer.source == '' ? "Please select a file... " : mediaPlayer.source
    }

    FileOpenBox {
        id: file_open_box
        nameFilters: [ "*" ]
        anchors.fill: parent
        anchors.margins: 16
        Component.onCompleted: {
            folder = "file:///usr/share/phytec-qtdemo/videos"
        }
        onOkClicked: {
            mediaPlayer.source = "file:" + selectedFilePath
            console.log( "set mediaPlayer.source = " + selectedFilePath )
        }
    }
}

Rectangle {
    id: toolbar
    color: "lightgrey"
    Layout.fillWidth: true
    Layout.preferredHeight: parent.height / 12
    RowLayout {
        anchors.fill: parent
        Button {
            font.pixelSize: parent.height * 0.5
            font.family: "FontAwesome"
            text: icons.fa_folder_open
            flat: true
            onPressed: file_open_box.open()
        }
        Button {
            font.pixelSize: parent.height * 0.5
            font.family: "FontAwesome"
            text: icons.fa_stop
            flat: true
            onPressed: mediaPlayer.stop()
        }
        Button {
            font.pixelSize: parent.height * 0.5
            font.family: "FontAwesome"
            text: icons.fa_pause
            flat: true
            onPressed: mediaPlayer.pause()
        }
        Button {
            font.pixelSize: parent.height * 0.5
            font.family: "FontAwesome"
            text: icons.fa_play
            flat: true
            onPressed: mediaPlayer.play()
        }
        ProgressBar {
            Layout.fillWidth: true
            Layout.margins: 10
            to: mediaPlayer.duration
            value: mediaPlayer.position
        }
    }
}
}
StdDialog{
    id:dialog
}
}//Item
