import QtQuick 2.3
import Qt.labs.folderlistmodel 2.1
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.2
import QtQuick 2.4
import QtQuick.Controls 2.0
import QtQuick 2.3
import QtQml.Models 2.2
import QtQuick.Window 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.2
import QtQuick 2.0
import QtQuick 2.2
import QtQuick.Layouts 1.2
ColumnLayout {
    id: layout
   // property alias mouseArea: mouseArea
    anchors.fill: parent
    spacing: 0

    ListModel {
        id: pagemenuviewer
        ListElement {
            name: "Image Viewer"
            icon: "\uf03e" //icons.fa_picture_o
            pageSource: "PageMenuViewer.qml"
        }
    }
    function selectItemHome() {
        var myModelItem = pagemenuviewer.get(configuredPages.model_index_map[view.currentIndex])
        client_area.clientUrl = myModelItem.pageSource
        client_area.title = myModelItem.name
        mainqml.color = "white"
        view.visible = false
      }

    Image {
        id: main
        source: "qrc:/images/M.png"
        Layout.fillWidth: true
        Layout.fillHeight: true

        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 10; leftMargin: 740; bottomMargin: parent.height - 60
            }
            height: 50
            id: powermenu1
            source: "qrc:/images/homeLogo.png"

        MouseArea{
            anchors.fill: parent
            onClicked: selectItemHome()
        }
      }
   }
}

