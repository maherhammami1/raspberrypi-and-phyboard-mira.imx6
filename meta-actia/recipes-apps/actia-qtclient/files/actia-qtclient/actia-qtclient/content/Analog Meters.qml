import QtQuick 2.2
import QtQuick.Layouts 1.2

ColumnLayout {
    id: layout
    anchors.fill: parent
    spacing: 0

    Menubar {}

    Rectangle {
        id: main
        color: "grey"
        Layout.fillWidth: true
        Layout.fillHeight: true

        //! [the dial in use]
        // Dial with a slider to adjust it
        Dial {
            id: dial
            anchors.centerIn: parent
            value: slider.x * 100 / (container.width - 32)
        }
        //scaleDial
        onHeightChanged: scaleControl()
        onWidthChanged: scaleControl()
        function scaleControl() {
            //scale to fit, max 1.0
            var min_scale = Math.min(main.width / dial.width, (main.height - container.height) / dial.height)
            dial.scale = Math.min(min_scale, 1.0)
        }
        //! [the dial in use]

        Rectangle {
            id: container
            property int oldWidth: 0
            anchors { bottom: parent.bottom; left: parent.left
                right: parent.right; leftMargin: 20; rightMargin: 20
                bottomMargin: 20
            }
            height: 20
            radius: 8
            opacity: 0.7
            antialiasing: true
            gradient: Gradient {
                GradientStop { position: 0.0; color: "gray" }
                GradientStop { position: 1.0; color: "white" }
            }
            readonly property int sliderSize: 38
            onWidthChanged: {
                if (oldWidth === 0) {
                    //items in layouts get the size during the
                    //second resize, the first resize has negative
                    //width values
                    if (width < 0)
                        return
                    oldWidth = width;
                    return
                }
                var desiredPercent = (slider.x) * 100 / (oldWidth - sliderSize)
                slider.x = (desiredPercent * (width - sliderSize) / 100)
                oldWidth = width
            }
            Rectangle {
                id: slider
                x: 1; y: 1; width: parent.sliderSize; height: 18
                radius: 6
                antialiasing: true
                gradient: Gradient {
                    GradientStop { position: 0.0; color: "#424242" }
                    GradientStop { position: 1.0; color: "black" }
                }
                MouseArea {
                    anchors.fill: parent
                    anchors.margins: -16 // Increase mouse area a lot outside the slider
                    drag.target: slider; drag.axis: Drag.XAxis
                    drag.minimumX: 2; drag.maximumX: container.width - container.sliderSize - 2
                }
            }
        }
    }
}
