/*import QtQuick 2.3
import Qt.labs.folderlistmodel 2.1
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.2
import QtQuick 2.4
import QtQuick.Controls 2.0
import QtQuick 2.3
import QtQml.Models 2.2
import QtQuick.Window 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.2
import QtQuick 2.0
import QtQuick 2.2
import QtQuick.Layouts 1.2
ColumnLayout {
    id: layout
   // property alias mouseArea: mouseArea
    anchors.fill: parent
    spacing: 0

    ListModel {
        id: pagemenuviewer
        ListElement {
            name: "Image Viewer"
            icon: "\uf03e" //icons.fa_picture_o
            pageSource: "PageMenuViewer.qml"
        }
    }
    function selectItemHome() {
        var myModelItem = pagemenuviewer.get(configuredPages.model_index_map[view.currentIndex])
        client_area.clientUrl = myModelItem.pageSource
        client_area.title = myModelItem.name
        mainqml.color = "white"
        view.visible = false
      }

        Image {
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 10; leftMargin: 740; bottomMargin: parent.height - 60
            }
            height: 85
            id: powermenu1
            source: "qrc:/images/homeLogo.png"

        MouseArea{
            anchors.fill: parent
            onClicked: selectItemHome()
        }
      }
        Text{
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 740; leftMargin: 10; bottomMargin: parent.height - 150
            }
            text: "VITESSE"
            font.family: "Helvetica"
            font.pointSize: 20
            color: "black"
        }
        Text{
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 740; leftMargin: 10; bottomMargin: parent.height - 250
            }
            text: "PUISSANCE MOTEUR"
            font.family: "Helvetica"
            font.pointSize: 20
            color: "black"
        }
        Text{
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 740; leftMargin: 10; bottomMargin: parent.height - 350
            }
            text: "TEMPERATURE"
            font.family: "Helvetica"
            font.pointSize: 20
            color: "black"
        }
        Text{
            anchors { bottom: parent.bottom; right: parent.right; left:parent.left
                rightMargin: 740; leftMargin: 10; bottomMargin: parent.height - 450
            }
            text: "LEDS"
            font.family: "Helvetica"
            font.pointSize: 20
            color: "black"
        }
   }*/
import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.2
import FriWare.ProcessIf 1.0
import IoTClient 1.0
import LED 1.0
import LEDNF 1.0
import LEDD 1.0
import LEDG 1.0
import LEDGD 1.0


Column {
    id: layout
    anchors.fill: parent
    spacing: 0

    IoTClient{
        id:client
    }
    LED{
        id:led
    }
    LEDNF{
        id:lednf
    }
    LEDD{
        id:ledd
    }
    LEDG{
        id:ledg
    }
    LEDGD{
        id:ledgd
    }

    Column {
        id:main
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height * 8 / 9
        Item {
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height * 0.04
        }
        Item {
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height * 0.1
            anchors.leftMargin: 20
            anchors.rightMargin: 20
            opacity: led1 ? 1 : 0.5
            Rectangle {
                anchors.fill: parent
                radius: 5
                color: "#EFF0F1"
                border.color: "grey"
                border.width: 2
            }
            Row {
                anchors.fill: parent
                anchors.leftMargin: 30
                spacing: 210
                Item {
                    anchors.verticalCenter: parent.verticalCenter
                    width: 40; height: 40
                    Rectangle {
                        width: 40; height: 40
                        id: circle1
                        color: "#00000000"
                        border.width: 2
                        border.color: "black"
                        radius: width*0.5
                    }
                    Rectangle {
                        anchors.centerIn: circle1
                        id: ui_led1
                        width: circle1.width - 12
                        height: circle1.height - 12
                        radius: width*0.5
                        color: ProcessIf.led1 ? "yellow" : "lightgrey"
                    }
                }
                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    text: "Clignotant Gauche"
                    font.pixelSize: circle1.height * 0.5
                }
                Switch {
                    anchors.verticalCenter: parent.verticalCenter
                    id: led1switch
                    //checked: client.putledclinogauche() ? true : false
                    //enabled: client.putledclinogauche() ? true : false
                    onCheckedChanged:
                    {client.gettemperature()
                        console.log("temp is = "+temperature.value)
                        message_text.text = qsTr("%1").arg(temperature.value)
                    }
                }
            }
        }
        Item {
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height * 0.04
        }
        Item {
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height * 0.1
            anchors.leftMargin: 20
            anchors.rightMargin: 20
            opacity: led2 ? 1 : 0.5
            Rectangle {
                anchors.fill: parent
                radius: 5
                color: "#EFF0F1"
                border.color: "grey"
                border.width: 2
            }
            Row {
                anchors.fill: parent
                anchors.leftMargin: 30
                spacing: 215
                Item {
                    anchors.verticalCenter: parent.verticalCenter
                    width: 40; height: 40
                    Rectangle {
                        width: 40; height: 40
                        id: circle2
                        color: "#00000000"
                        border.width: 2
                        border.color: "black"
                        radius: width*0.5
                    }
                    Rectangle {
                        anchors.centerIn: circle2
                        id: ui_led2
                        width: circle2.width - 12
                        height: circle2.height - 12
                        radius: width*0.5
                        //color: ProcessIf.led2 ? "yellow" : "lightgrey"
                    }
                }
                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    text: "Clignotant Droite"
                    font.pixelSize: circle2.height * 0.5
                }
                Switch {
                    anchors.verticalCenter: parent.verticalCenter
                    id: led2switch
                    checked: ProcessIf.led2 ? true : false
                    enabled: led2 ? true : false
                    onCheckedChanged: {

                    }
                }
            }
        }
        Item {
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height * 0.04
        }
        Item {
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height * 0.1
            anchors.leftMargin: 20
            anchors.rightMargin: 20
            opacity: led3 ? 1 : 0.5
            Rectangle {
                anchors.fill: parent
                radius: 5
                color: "#EFF0F1"
                border.color: "grey"
                border.width: 2
            }
            Row {
                anchors.fill: parent
                anchors.leftMargin: 30
                spacing: 265
                Item {
                    anchors.verticalCenter: parent.verticalCenter
                    width: 40; height: 40
                    Rectangle {
                        width: 40; height: 40
                        id: circle3
                        color: "#00000000"
                        border.width: 2
                        border.color: "black"
                        radius: width*0.5
                    }
                    Rectangle {
                        anchors.centerIn: circle3
                        id: ui_led3
                        width: circle3.width - 12
                        height: circle3.height - 12
                        radius: width*0.5
                        color: ProcessIf.led3 ? "yellow" : "lightgrey"
                    }
                }
                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    text: "Phare"
                    font.pixelSize: circle3.height * 0.5
                }
                Switch {
                    anchors.verticalCenter: parent.verticalCenter
                    id: led3switch
                    //checked: ProcessIf.led3 ? true : false
                    //enabled: led3 ? true : false
                    onCheckedChanged: {
                    }
                }
            }
        }
        Item {
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height * 0.04
        }
        Item {
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height * 0.20
            anchors.bottomMargin: parent.bottom
            anchors.leftMargin: 20
            anchors.rightMargin: 20
            Rectangle {
                anchors.fill: parent
                radius: 5
                color: "#EFF0F1"
                border.color: "grey"
                border.width: 2
            }
            Text {
                id : message_text
                anchors.centerIn: parent
                text: "This page shows how to control user data."
                font.pixelSize: parent.height * 0.2
            }
        }
        Item {
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height * 0.04
        }
    }
}


