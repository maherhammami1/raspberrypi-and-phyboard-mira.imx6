import QtQuick 2.3
import QtQml.Models 2.2
import QtQuick.Window 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.2
import IoTClient 1.0
import LED 1.0
import "content"

Window {
    visible: true
    // a fixed size is required by qt 5.7 2d renderer with linuxfb
    width: 800
    height: 480
    id: mainqml
    title: qsTr("phytec-qtdemo")
    color: "grey"

    property int itemAngle: 55
    property int itemSize: width *0.7

    Component.onCompleted: mainqml.showMaximized()

    ListModel {
        id: page_model
        ListElement {
            name: "Image Viewer"
            icon: "\uf03e" //icons.fa_picture_o
            pageSource: "content/PageMenuViewer.qml"
        }
    }

    Component {
        id: page_delegate
        Item {
            id: pageItem
            width: itemSize * 0.8
            height: mainqml.height * 0.8
            scale: PathView.pageScale
            opacity: PathView.pageOpacity
            z: PathView.z
            property real rotY:  PathView.rotY
            property real rotCenter:  PathView.rotCenter
            property int rotShade:  PathView.rotShade
            property real visShade:  PathView.visShade
            MouseArea {
                anchors.fill: parent
                onClicked: selectCurrentItem()
            }
            Image {
                anchors.horizontalCenter: pageItem.horizontalCenter
                anchors.verticalCenter: pageItem.verticalCenter
                anchors.fill: parent
                id: startimage
                source: "qrc:/images/renault.png"
                width:  itemSize
                height: mainqml.height
                  }
            Rectangle {
                //rotation shadow background effect
                anchors.horizontalCenter: pageItem.horizontalCenter
                anchors.verticalCenter: pageItem.verticalCenter
                height: parent.width + 2
                width: parent.height + 2
                radius: pageItem.height / 10
                rotation: rotShade
                opacity: visShade
                gradient: Gradient {
                    GradientStop { position: 0.0; color: "#000000" }
                    GradientStop { position: 1.0; color: "#00000000" }
                }
                visible: doExpensiveDrawOperations
            }
            Rectangle {
                //mirror shadow below
                anchors.horizontalCenter: pageItem.horizontalCenter
                anchors.top: pageItem.top
                anchors.topMargin: parent.height * 1.2
                height: parent.height + 2
                width: parent.width + 2
                radius: pageItem.height / 10
                rotation: 180
                opacity: 0.1
                gradient: Gradient {
                    GradientStop { position: 0.5; color: "#00000000" }
                    GradientStop { position: 1.0; color: "#000000" }
                }
            }
        }
    }

    function selectCurrentItem() {
        var myModelItem = page_model.get(configuredPages.model_index_map[view.currentIndex])
        client_area.clientUrl = myModelItem.pageSource
        client_area.title = myModelItem.name
        mainqml.color = "white"
        view.visible = false
    }
    function exitCurrentItem() {
        client_area.clientUrl = ""
        mainqml.color = "grey"
        view.focus = true
        view.visible = true
    }

    DelegateModel {
        id: displayDelegateModel
        delegate: page_delegate
        model: page_model

        groups: [
            DelegateModelGroup {
                id: configuredPages
                //includeByDefault: true
                name: "configured"
                property var model_index_map: []
            }
        ]
        filterOnGroup: "configured"
        Component.onCompleted: {
            var count = page_model.count;
            for(var i = 0;i < count;i++) {
                var entry = page_model.get(i);
                if(enabledPages.indexOf(entry.name) >= 0) {
                    items.insert(entry, "configured");
                    var t = configuredPages.model_index_map
                    t.push(i)
                    configuredPages.model_index_map = t
                }
            }
        }
    }

    PathView {
        id: view
        anchors.fill: parent
        model: displayDelegateModel
        preferredHighlightBegin: 0.5
        preferredHighlightEnd: 0.5
        snapMode: PathView.SnapToItem
        property Item currentItem
        Rectangle {
            id:background
            anchors.fill: parent
            gradient: Gradient {
                GradientStop { position: 0.0; color: "black" }
                GradientStop { position: 1.0; color: "grey" }
            }
            visible: doExpensiveDrawOperations
        }
        path: Path {
            startX: 0
            startY: height/2
            PathPercent { value: 0.0 }

            PathAttribute { name: "pageOpacity"; value: 0.2 }
            PathAttribute { name: "pageScale"; value: 0.4 }
            PathAttribute { name: "rotY"; value: itemAngle }
            PathAttribute { name: "rotCenter"; value: 0 }
            PathAttribute { name: "visShade"; value: 1.0 }
            PathAttribute { name: "rotShade"; value: 90 }
            PathAttribute { name: "z"; value: 0 }

            PathLine {
                x: width / 2 - itemSize / 3
                y: height / 2
            }
            PathAttribute { name: "rotY"; value: itemAngle }
            PathAttribute { name: "rotCenter"; value: 0 }
            PathAttribute { name: "visShade"; value: 1.0 }
            PathAttribute { name: "rotShade"; value: 90 }
            PathAttribute { name: "z"; value: 10 }

            PathLine { relativeX: 0; relativeY: 0 }
            PathAttribute { name: "pageScale"; value: 0.8 }
            PathPercent { value: 0.40 }

            PathLine { x: width/2
                       y: height/2
            }
            PathAttribute { name: "rotY"; value: 0 }
            PathAttribute { name: "pageScale"; value: 1 }
            PathAttribute { name: "pageOpacity"; value: 1 }
            PathAttribute { name: "z"; value: 20 }
            PathAttribute { name: "rotCenter"; value: 0 }
            PathAttribute { name: "visShade"; value: 0 }
            PathAttribute { name: "rotShade"; value: 90 }

            PathLine { relativeX: 0; relativeY: 0 }
            PathAttribute { name: "rotCenter"; value: itemSize}
            PathAttribute { name: "rotShade"; value: -90 }

            PathLine {
                x: view.width / 2 + itemSize / 3
                y: height/2
            }
            PathAttribute { name: "pageScale"; value: 1.0 }
            PathPercent { value: 0.60 }

            PathLine { relativeX: 0; relativeY: 0 }
            PathAttribute { name: "rotY"; value: -itemAngle }
            PathAttribute { name: "z"; value: 10 }
            PathAttribute { name: "visShade"; value: 1.0 }

            PathLine {
                x: view.width //+ itemSize / 10
                y: view.height / 2
            }
            PathPercent { value: 1.0 }
            PathAttribute { name: "pageOpacity"; value: 0.2 }
            PathAttribute { name: "pageScale"; value: 0.4 }
            PathAttribute { name: "rotY"; value: -itemAngle }
            PathAttribute { name: "rotCenter"; value: itemSize }
            PathAttribute { name: "visShade"; value: 0.8 }
            PathAttribute { name: "rotShade"; value: -90 }

        }

        // Keyboard control
        focus: true
        Keys.onLeftPressed: decrementCurrentIndex()
        Keys.onRightPressed: incrementCurrentIndex()
        Keys.onReturnPressed: selectCurrentItem()
        Keys.onEscapePressed: exitCurrentItem()
    }

    Image {
        id: icon_loading
        anchors.centerIn: parent
        source: "qrc:/images/icon_loading.png"
        visible: false
        NumberAnimation on rotation {
             from: 0
             to: 360
             running: icon_loading.visible == true
             loops: Animation.Infinite
             duration: 900
         }
    }

    // Client Items are displayed inside this item
    // they are shown or not by setting the visibility
    Item {
        id: client_area
        anchors.fill: parent
        visible: false
        clip: true
        property url clientUrl
        property string title

        onClientUrlChanged: {
            visible = (clientUrl == '' ? false : true)
        }

        MouseArea {
            anchors.fill: parent
            enabled: client_area.visible
            // eat mouse events
        }
        Loader {
            id: client_loader
            focus: true
            source: client_area.clientUrl
            anchors.fill: parent
            asynchronous: true  // must be true to get the "Loading" Status
            onStatusChanged: {
                if( status === Loader.Loading ) {
                    icon_loading.visible = true
                }
                else {
                    icon_loading.visible = false
                }
            }
        }
    }

    IconFonts {
        id: icons
    }
}
