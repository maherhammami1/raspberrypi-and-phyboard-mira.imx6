import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.2

Rectangle {
    id: menubar
    color: "#FFFBFF"
    Layout.fillWidth: true
    Layout.preferredHeight: parent.height / 9

    Button{
        id:pagegetbutton
        anchors { bottom: parent.bottom; left: parent.left
            right: parent.right; rightMargin: parent.width /2
            bottomMargin: 0
                }
        text: "GET"
        height: parent.height
        opacity: 1
        antialiasing: true
        onClicked: {
            putbuttonright.visible = false
            putbuttonleft.visible =false
            putbuttoncenter.visible = false
            container.visible = false
            container1.visible = false
            container2.visible = false
            pagegetbutton.checked = true
            pageputbutton.checked = false
            main.source = "qrc:/images/1.jpeg"
        }
    }

    Button{
        id: pageputbutton
        anchors { bottom: parent.bottom; left: parent.left
            right: parent.right; leftMargin: parent.width /2
            bottomMargin: 0
                }
        text: "PUT"
        height: parent.height
        opacity: 1
        antialiasing: true
        onClicked: {
        putbuttonright.visible = true
        putbuttonleft.visible =true
        putbuttoncenter.visible = true
        container.visible = true
        container1.visible = true
        container2.visible = true
        pagegetbutton.checked = false
        pageputbutton.checked = true
        main.source = "qrc:/images/3.jpeg"
        }
    }
    /*Button {
        anchors.verticalCenter: parent.verticalCenter
        font.family: "FontAwesome"
        font.pixelSize: parent.height * 0.5
        text: icons.fa_angle_left
        flat: true
        onClicked: mainqml.exitCurrentItem()
    }
    Button {
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        font.family: "FontAwesome"
        font.pixelSize: parent.height * 0.5
        text: icons.fa_angle_right
        flat: true
        onClicked: mainqml.exitCurrentItem()
    }
    Label {
        id: title
        anchors.centerIn: menubar
        font.pixelSize: parent.height * 0.5
        text: "ACTIA"
    }*/
    /*Rectangle {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        width: parent.width
        height: 4
        gradient: Gradient {
          GradientStop { position: 0.0; color: "#FFFBFF" }
          GradientStop { position: 1.0; color: "grey" }
        }
    }*/
}

