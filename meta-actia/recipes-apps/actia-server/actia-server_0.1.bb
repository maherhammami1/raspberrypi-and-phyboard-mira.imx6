#
# This file was derived from the 'actia-server' example recipe in the
# Yocto Project Development Manual.
#

PR = "r1"
SUMMARY = "actia server"
DESCRIPTION = ""
HOMEPAGE = "https://www.iotivity.org/"
DEPENDS = "iotivity mraa"
SECTION = "server"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://actia-server.tar.gz \
	   file://actia-server.service \
          "

#FILE += "actia-server.service"

inherit pkgconfig systemd

TARGET_CC_ARCH += "${LDFLAGS}"

S = "${WORKDIR}/ACTIA-QTserver"

IOTIVITY_BIN_DIR = "/opt/iotivity"
IOTIVITY_BIN_DIR_D = "${D}${IOTIVITY_BIN_DIR}"

do_compile_prepend() {
    export PKG_CONFIG_PATH="${PKG_CONFIG_PATH}"
    export PKG_CONFIG="PKG_CONFIG_SYSROOT_DIR=\"${PKG_CONFIG_SYSROOT_DIR}\" pkg-config"
    export LD_FLAGS="${LD_FLAGS}"
}

do_install() {
    install -d ${IOTIVITY_BIN_DIR_D}/apps/ACTIA-QTserver
    install -c -m 555 ${S}/ACTIA-QTserver ${IOTIVITY_BIN_DIR_D}/apps/ACTIA-QTserver
    install -Dm 0644 ${WORKDIR}/actia-server.service ${D}${systemd_system_unitdir}/actia-server.service
}

SYSTEMD_SERVICE_${PN} = "actia-server.service"

SYSTEMD_AUTO_ENABLE = "enable"

FILES_${PN} = "${IOTIVITY_BIN_DIR}/apps/ACTIA-QTserver/ACTIA-QTserver ${systemd_unitdir}" 
FILES_${PN}-dbg = "${IOTIVITY_BIN_DIR}/apps/ACTIA-QTserver/.debug"
RDEPENDS_${PN} += "iotivity-resource mraa"
BBCLASSEXTEND = "native nativesdk"
PACKAGE_DEBUG_SPLIT_STYLE = "debug-without-src"
