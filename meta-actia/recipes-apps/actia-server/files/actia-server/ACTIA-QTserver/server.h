//******************************************************************
//
// Copyright 2014 Intel Corporation.
//
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

#ifndef SERVER_H_
#define SERVER_H_

#include <string>
#include <iostream>
#include <memory>
#include "ocstack.h"
#include "observer.h"
#include "OCPlatform.h"
#include "OCApi.h"

using namespace std;
using namespace OC;

class IoTServer
{
    shared_ptr<PlatformConfig> m_platformConfig;
    // OCRepresentation for temperature
    OCRepresentation m_temperatureRepresentation;
    OCResourceHandle m_temperatureResource;
    // OCRepresentation for gaz
    OCRepresentation m_gazRepresentation;
    OCResourceHandle m_gazResource;
    // OCRepresentation for humidity
    OCRepresentation m_humidityRepresentation;
    OCResourceHandle m_humidityResource;
    // OCRepresentation for pression
    OCRepresentation m_pressionRepresentation;
    OCResourceHandle m_pressionResource;
    // OCRepresentation for led
    OCRepresentation m_ledRepresentation;
    OCResourceHandle m_ledResource;
    // OCRepresentation for ledd
    OCRepresentation m_leddRepresentation;
    OCResourceHandle m_leddResource;
    // OCRepresentation for ledg
    OCRepresentation m_ledgRepresentation;
    OCResourceHandle m_ledgResource;
    // OCRepresentation for ledgd
    OCRepresentation m_ledgdRepresentation;
    OCResourceHandle m_ledgdResource;
    // OCRepresentation for moteur
    OCRepresentation m_moteurRepresentation;
    OCResourceHandle m_moteurResource;
    // observationIds temperature
    ObservationIds m_temperatureObservers;
    shared_ptr<IoTObserver> m_temperatureObserverLoop;
    // observationIds gaz
    ObservationIds m_gazObservers;
    shared_ptr<IoTObserver> m_gazObserverLoop;
    // observationIds pression
    ObservationIds m_pressionObservers;
    shared_ptr<IoTObserver> m_pressionObserverLoop;
    // observationIds humidity
    ObservationIds m_humidityObservers;
    shared_ptr<IoTObserver> m_humidityObserverLoop;

    void initializePlatform();
    void setupResources();
    void createResource(string, string, EntityHandler, OCResourceHandle&);

    OCRepresentation getTemperatureRepresentation();
    OCRepresentation getGazRepresentation();
    OCRepresentation getPressionRepresentation();
    OCRepresentation getHumidityRepresentation();
    OCRepresentation getLEDRepresentation();
    OCRepresentation getLEDDRepresentation();
    OCRepresentation getLEDGRepresentation();
    OCRepresentation getLEDGDRepresentation();
    OCRepresentation getMOTEURRepresentation();

    void putLEDRepresentation();
    void putLEDDRepresentation();
    void putLEDGRepresentation();
    void putLEDGDRepresentation();
    void putMOTEURRepresentation();

    //Polling threads to periodically query sensor values and notify
    //subscribers.
    void temperatureObserverLoop();
    void gazObserverLoop();
    void pressionObserverLoop();
    void humidityObserverLoop();

    OCEntityHandlerResult temperatureEntityHandler(shared_ptr<OCResourceRequest>);
    OCEntityHandlerResult gazEntityHandler(shared_ptr<OCResourceRequest>);
    OCEntityHandlerResult pressionEntityHandler(shared_ptr<OCResourceRequest>);
    OCEntityHandlerResult humidityEntityHandler(shared_ptr<OCResourceRequest>);
    OCEntityHandlerResult LEDEntityHandler(shared_ptr<OCResourceRequest>);
    OCEntityHandlerResult LEDDEntityHandler(shared_ptr<OCResourceRequest>);
    OCEntityHandlerResult LEDGEntityHandler(shared_ptr<OCResourceRequest>);
    OCEntityHandlerResult LEDGDEntityHandler(shared_ptr<OCResourceRequest>);
    OCEntityHandlerResult MOTEUREntityHandler(shared_ptr<OCResourceRequest>);
public:
    IoTServer();
    virtual ~IoTServer();
};

#endif /* SERVER_H_ */
