//******************************************************************
//
// Copyright 2014 Intel Corporation.
//
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <math.h>
#include "mraa.h"
#include "mraa.hpp"
#include <mraa/types.h>
#include <sys/mman.h>
#include <fstream>
#include <cstdio>

#define ONBOARD_LED_PIN1 25
#define ONBOARD_LED_PIN2 26
#define ONBOARD_LED_PIN3 40
#define ONBOARD_IN1_PIN 27
#define ONBOARD_IN2_PIN 28
#define ONBOARD_IN3_PIN 30
#define ONBOARD_IN4_PIN 32
#define TEMPERATURE_AIO_PIN 58
#define PRESSION_AIO_PIN 52
#define GAZ_AIO_PIN 50
#define HUMIDITY_AIO_PIN 49
#define SAMPLE_NUM 5

namespace Sensors
{
mraa_gpio_context led_gpio1 = NULL;
mraa_gpio_context led_gpio2 = NULL;
mraa_gpio_context led_gpio3 = NULL;
mraa_gpio_context in1_gpio = NULL;
mraa_gpio_context in2_gpio = NULL;
mraa_gpio_context in3_gpio = NULL;
mraa_gpio_context in4_gpio = NULL;
mraa_aio_context tmp_aio = NULL;
mraa_aio_context gaz_aio = NULL;
mraa_aio_context hum_aio = NULL;
mraa_aio_context pres_aio = NULL;

int val;

inline void SetupPins()
{
	//ONBOARD_LED_PIN1
    led_gpio1 = mraa_gpio_init(ONBOARD_LED_PIN1); // Initialize pin 25
    if (led_gpio1 != NULL)
        {mraa_gpio_dir(led_gpio1, MRAA_GPIO_OUT);}
    else
	{printf("LED_PIN not initialized\n");} // Set direction to OUTPUT
	//ONBOARD_LED_PIN2
    led_gpio2 = mraa_gpio_init(ONBOARD_LED_PIN2); // Initialize pin 26
    if (led_gpio2 != NULL)
        {mraa_gpio_dir(led_gpio2, MRAA_GPIO_OUT);}
    else
	{printf("LED_PIN not initialized\n");} // Set direction to OUTPUT
	//ONBOARD_LED_PIN3
    led_gpio3 = mraa_gpio_init(ONBOARD_LED_PIN3); // Initialize pin 40
    if (led_gpio3 != NULL)
        {mraa_gpio_dir(led_gpio3, MRAA_GPIO_OUT);}
    else
	{printf("LED_PIN not initialized\n");} // Set direction to OUTPUT
	//ONBOARD_IN1_PIN
    in1_gpio = mraa_gpio_init(ONBOARD_IN1_PIN); // Initialize pin 27
    if (in1_gpio != NULL)
        {mraa_gpio_dir(in1_gpio, MRAA_GPIO_OUT);}
    else
	{printf("LED_PIN not initialized\n");} // Set direction to OUTPUT
	//ONBOARD_IN2_PIN
    in2_gpio = mraa_gpio_init(ONBOARD_IN2_PIN); // Initialize pin 28
    if (in2_gpio != NULL)
        {mraa_gpio_dir(in2_gpio, MRAA_GPIO_OUT);}
    else
	{printf("LED_PIN not initialized\n");} // Set direction to OUTPUT
	//ONBOARD_IN3_PIN
    in3_gpio = mraa_gpio_init(ONBOARD_IN3_PIN); // Initialize pin 30
    if (in3_gpio != NULL)
        {mraa_gpio_dir(in3_gpio, MRAA_GPIO_OUT);}
    else
	{printf("LED_PIN not initialized\n");} // Set direction to OUTPUT
	//ONBOARD_IN4_PIN
    in4_gpio = mraa_gpio_init(ONBOARD_IN4_PIN); // Initialize pin 32
    if (in4_gpio != NULL)
        {mraa_gpio_dir(in4_gpio, MRAA_GPIO_OUT);}
    else
	{printf("LED_PIN not initialized\n");} // Set direction to OUTPUT

    tmp_aio = mraa_aio_init(TEMPERATURE_AIO_PIN);     // initialize pin 0
    gaz_aio = mraa_aio_init(GAZ_AIO_PIN);             // initialize pin 2
    pres_aio = mraa_aio_init(PRESSION_AIO_PIN);       // initialize pin 1
    hum_aio = mraa_aio_init(HUMIDITY_AIO_PIN);        // initialize pin 3
}

inline void ClosePins()
{
    mraa_gpio_close(led_gpio1);
    mraa_gpio_close(led_gpio2);
    mraa_gpio_close(led_gpio3);
    mraa_gpio_close(in1_gpio);
    mraa_gpio_close(in2_gpio);
    mraa_gpio_close(in3_gpio);
    mraa_gpio_close(in4_gpio);


    mraa_aio_close(tmp_aio);
    mraa_aio_close(gaz_aio);
    mraa_aio_close(hum_aio);
    mraa_aio_close(pres_aio);
}

inline void SetOnboardLed(int on)
{
    if (led_gpio1 == NULL)
    {
        led_gpio1 = mraa_gpio_init(ONBOARD_LED_PIN1); // Initialize pin 25
        if (led_gpio1 != NULL)
            mraa_gpio_dir(led_gpio1, MRAA_GPIO_OUT); // Set direction to OUTPUT
    }
    if (led_gpio1 != NULL)
        mraa_gpio_write(led_gpio1, on); // Writes into GPIO
}


inline void SetOnboardLedclinodroit(int on)
{
    if (led_gpio2 == NULL)
    {
        led_gpio2 = mraa_gpio_init(ONBOARD_LED_PIN2); // Initialize pin 27
        if (led_gpio2 != NULL)
            mraa_gpio_dir(led_gpio2, MRAA_GPIO_OUT); // Set direction to OUTPUT
    }
    if(led_gpio2 != NULL){
        	mraa_gpio_write(led_gpio2, on);      // Writes into GPIO
		sleep(1);
		mraa_gpio_write(led_gpio2, 0);
		sleep(1);
	}
}


inline void SetOnboardLedclinogauche(int on)
{
    if (led_gpio3 == NULL)
    {
        led_gpio3 = mraa_gpio_init(ONBOARD_LED_PIN3); // Initialize pin 27
        if (led_gpio3 != NULL)
            mraa_gpio_dir(led_gpio3, MRAA_GPIO_OUT); // Set direction to OUTPUT
    }
    if (led_gpio3 != NULL){
        		mraa_gpio_write(led_gpio3, on);         // Writes into GPIO
			sleep(1);
			mraa_gpio_write(led_gpio3, 0);
			sleep(1);
	}
}

inline void setOnboardfourclino(int on)
{
	if (led_gpio2 == NULL && led_gpio3 == NULL)
		{
			led_gpio3 = mraa_gpio_init(ONBOARD_LED_PIN3);	
			led_gpio2 = mraa_gpio_init(ONBOARD_LED_PIN2);
			if (led_gpio2 != NULL && led_gpio3 != NULL)
				{
					mraa_gpio_dir(led_gpio3, MRAA_GPIO_OUT);
					mraa_gpio_dir(led_gpio2, MRAA_GPIO_OUT);
				}	
		}
	if (led_gpio2 != NULL && led_gpio3 != NULL)
		{
			mraa_gpio_write(led_gpio3, on);
			mraa_gpio_write(led_gpio2, on);
			sleep(1);
			mraa_gpio_write(led_gpio3, 0);
			mraa_gpio_write(led_gpio2, 0);
			sleep(1);	
			
		}


}

inline void Setfoursteps(int on)
{
	if(in1_gpio == NULL && in2_gpio == NULL && in3_gpio == NULL && in4_gpio == NULL)
		{
			in1_gpio = mraa_gpio_init(ONBOARD_IN1_PIN);
			in2_gpio = mraa_gpio_init(ONBOARD_IN2_PIN);
			in3_gpio = mraa_gpio_init(ONBOARD_IN3_PIN);
			in4_gpio = mraa_gpio_init(ONBOARD_IN4_PIN);
			if(in1_gpio != NULL && in2_gpio != NULL && in3_gpio != NULL && in4_gpio != NULL)
				{
					 mraa_gpio_dir(in1_gpio, MRAA_GPIO_OUT);
					 mraa_gpio_dir(in2_gpio, MRAA_GPIO_OUT);
 					 mraa_gpio_dir(in3_gpio, MRAA_GPIO_OUT);
					 mraa_gpio_dir(in4_gpio, MRAA_GPIO_OUT);
				}	
		}
	if(in1_gpio != NULL && in2_gpio != NULL && in3_gpio != NULL && in4_gpio != NULL)
		{
					mraa_gpio_write(in1_gpio, on);
					mraa_gpio_write(in2_gpio, on);
					mraa_gpio_write(in3_gpio, 0);
					mraa_gpio_write(in4_gpio, 0);
					usleep(5000);
					mraa_gpio_write(in1_gpio, 0);
					mraa_gpio_write(in2_gpio, on);
					mraa_gpio_write(in3_gpio, on);
					mraa_gpio_write(in4_gpio, 0);
					usleep(5000);
				    	mraa_gpio_write(in1_gpio, 0);
					mraa_gpio_write(in2_gpio, 0);
					mraa_gpio_write(in3_gpio, on);
					mraa_gpio_write(in4_gpio, on);
					usleep(5000);
					mraa_gpio_write(in1_gpio, on);
					mraa_gpio_write(in2_gpio, 0);
					mraa_gpio_write(in3_gpio, 0);
					mraa_gpio_write(in4_gpio, on);
					usleep(5000);
		}
	
}

inline float GetAverageTemperatureRaw()
{
    if (tmp_aio == NULL)
    {
        tmp_aio = mraa_aio_init(TEMPERATURE_AIO_PIN); // initialize pin 0
    }
    
    uint16_t adc_value = 0;
    for (int i=0; i< SAMPLE_NUM; i++)
        adc_value += mraa_aio_read(tmp_aio);          // read the raw value
    
    float average = (float)adc_value/SAMPLE_NUM;
    cout << "Temperature reading raw ..."  << average << endl;
    
    return average;
}
inline float GetAveragePressionRaw()
{
    if (pres_aio == NULL)
    {
        pres_aio = mraa_aio_init(PRESSION_AIO_PIN); // initialize pin 1
    }
    
    uint16_t adc_value = 0;
    for (int i=0; i< SAMPLE_NUM; i++)
        adc_value += mraa_aio_read(pres_aio);           // read the raw value
    
    float average = (float)adc_value/SAMPLE_NUM;
    cout << "Pression reading raw ..."  << average << endl;
    
    return average;
}
inline float GetAverageGazRaw()
{
    if (gaz_aio == NULL)
    {
        gaz_aio = mraa_aio_init(GAZ_AIO_PIN); // initialize pin 2
    }
    
    uint16_t adc_value = 0;
    for (int i=0; i< SAMPLE_NUM; i++)
        adc_value += mraa_aio_read(gaz_aio);           // read the raw value
    
    float average = (float)adc_value/SAMPLE_NUM;
    cout << "Gaz reading raw ..."  << average << endl;
    
    return average;
}
inline float GetAverageHumidityRaw()
{
    if (hum_aio == NULL)
    {
        hum_aio = mraa_aio_init(HUMIDITY_AIO_PIN); // initialize pin 3
    }
    
    uint16_t adc_value = 0;
    for (int i=0; i< SAMPLE_NUM; i++)
        adc_value += mraa_aio_read(hum_aio);           // read the raw value
    
    float average = (float)adc_value/SAMPLE_NUM;
    cout << "Humidity reading raw ..."  << average << endl;
    
    return average;
}

inline float GetTemperatureInC()
{
    // Temperature calculation using simpilfy Steinhart-Hart equation
    //
    //          1/T = 1/T0 + 1/beta*ln (R/R0)
    //
    // where T0 = 25C room temp, R0 = 10000 ohms
    //
    float beta = 4090.0;            //the beta of the thermistor, magic number
    float t_raw = GetAverageTemperatureRaw();
    float R = 1023.0/t_raw -1;      // 
    R = 10000.0/R;                  // 10K resistor divider circuit
        
    float T1 = log(R/10000.0)/beta; // natural log 
    float T2 = T1 + 1.0/298.15;     // room temp 25C= 298.15K
    float ret = 1.0/T2 - 273.0;
 
    return ret;
}
}
