//******************************************************************
//
// Copyright 2014 Intel Corporation.
//
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

#ifndef NAMEDEFS_H_
#define NAMEDEFS_H_

#define EDISON_RESOURCE_INTERFACE "oic.if.rw"
#define TEMPERATURE_RESOURCE_TYPE "core.temperature"
#define HUMIDITY_RESOURCE_TYPE "core.humidity"
#define PRESSION_RESOURCE_TYPE "core.pression"
#define GAZ_RESOURCE_TYPE "core.gaz"
#define LED_RESOURCE_TYPE "core.led"
#define LEDD_RESOURCE_TYPE "core.ledd"
#define LEDG_RESOURCE_TYPE "core.ledg"
#define LEDGD_RESOURCE_TYPE "core.ledgd"
#define MOTEUR_RESOURCE_TYPE "core.mateur"
#define TEMPERATURE_RESOURCE_ENDPOINT "/temperature"
#define HUMIDITY_RESOURCE_ENDPOINT "/humidity"
#define PRESSION_RESOURCE_ENDPOINT "/pression"
#define GAZ_RESOURCE_ENDPOINT "/gaz"
#define LED_RESOURCE_ENDPOINT "/led"
#define LEDD_RESOURCE_ENDPOINT "/ledd"
#define LEDG_RESOURCE_ENDPOINT "/ledg"
#define LEDGD_RESOURCE_ENDPOINT "/ledgd"
#define MOTEUR_RESOURCE_ENDPOINT "/moteur"
#define TEMPERATURE_RESOURCE_KEY "temperature"
#define HUMIDITY_RESOURCE_KEY "humidity"
#define PRESSION_RESOURCE_KEY "pression"
#define GAZ_RESOURCE_KEY "gaz"
#define MOTEUR_RESOURCE_KEY "switch"
#define LED_RESOURCE_KEY "switch"
#define LEDD_RESOURCE_KEY "switch"
#define LEDG_RESOURCE_KEY "switch"
#define LEDGD_RESOURCE_KEY "switch"
#endif /* NAMEDEFS_H_ */
