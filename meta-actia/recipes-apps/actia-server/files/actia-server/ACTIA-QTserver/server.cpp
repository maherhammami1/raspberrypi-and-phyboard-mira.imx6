//******************************************************************
//
// Copyright 2014 Intel Corporation.
//
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include <signal.h>
#include <thread>
#include <functional>

#include "server.h"
#include "sensors.h"
#include "namedefs.h"
using namespace Sensors;


void IoTServer::initializePlatform()
{
    cout << "Running IoTServer constructor" << endl;
    m_platformConfig = make_shared<PlatformConfig>(ServiceType::InProc, ModeType::Server, "0.0.0.0",
                                                   0, OC::QualityOfService::HighQos);
    OCPlatform::Configure(*m_platformConfig);
}

IoTServer::IoTServer()
{
    initializePlatform();
    setupResources();
    m_temperatureRepresentation.setValue(TEMPERATURE_RESOURCE_KEY, (float) 0.0f);
    m_gazRepresentation.setValue(GAZ_RESOURCE_KEY, (float) 0.0f);
    m_pressionRepresentation.setValue(PRESSION_RESOURCE_KEY, (float) 0.0f);
    m_humidityRepresentation.setValue(HUMIDITY_RESOURCE_KEY, (float) 0.0f);
    m_ledRepresentation.setValue(LED_RESOURCE_KEY, 0);
    m_leddRepresentation.setValue(LEDD_RESOURCE_KEY, 0);
    m_ledgRepresentation.setValue(LEDG_RESOURCE_KEY, 0);
    m_ledgdRepresentation.setValue(LEDGD_RESOURCE_KEY, 0);
    m_moteurRepresentation.setValue(MOTEUR_RESOURCE_KEY, 0);
    SetupPins();
}

IoTServer::~IoTServer()
{
    cout << "Running IoTServer destructor" << endl;
    ClosePins();
}

void IoTServer::setupResources()
{
    //temperature
    EntityHandler cb1 = bind(&IoTServer::temperatureEntityHandler, this, placeholders::_1);
    createResource(TEMPERATURE_RESOURCE_ENDPOINT, TEMPERATURE_RESOURCE_TYPE, cb1,
                   m_temperatureResource);
    IoTObserverCb tempObsCb = bind(&IoTServer::temperatureObserverLoop, this);
    m_temperatureObserverLoop = make_shared<IoTObserver>(tempObsCb);
    //gaz
    EntityHandler cb2 = bind(&IoTServer::gazEntityHandler, this, placeholders::_1);
    createResource(GAZ_RESOURCE_ENDPOINT, GAZ_RESOURCE_TYPE, cb2,
                   m_gazResource);
    IoTObserverCb gazObsCb = bind(&IoTServer::gazObserverLoop, this);
    m_gazObserverLoop = make_shared<IoTObserver>(gazObsCb);
    //humidity
    EntityHandler cb4 = bind(&IoTServer::humidityEntityHandler, this, placeholders::_1);
    createResource(HUMIDITY_RESOURCE_ENDPOINT, HUMIDITY_RESOURCE_TYPE, cb4,
                   m_humidityResource);
    IoTObserverCb humObsCb = bind(&IoTServer::humidityObserverLoop, this);
    m_humidityObserverLoop = make_shared<IoTObserver>(humObsCb);
    //pression
    EntityHandler cb5 = bind(&IoTServer::pressionEntityHandler, this, placeholders::_1);
    createResource(PRESSION_RESOURCE_ENDPOINT, PRESSION_RESOURCE_TYPE, cb5,
                   m_pressionResource);
    IoTObserverCb presObsCb = bind(&IoTServer::pressionObserverLoop, this);
    m_pressionObserverLoop = make_shared<IoTObserver>(presObsCb);
    //led
    EntityHandler cb3 = bind(&IoTServer::LEDEntityHandler, this, placeholders::_1);
    createResource(LED_RESOURCE_ENDPOINT, LED_RESOURCE_TYPE, cb3, m_ledResource);
    //ledd
    EntityHandler cb6 = bind(&IoTServer::LEDDEntityHandler, this, placeholders::_1);
    createResource(LEDD_RESOURCE_ENDPOINT, LEDD_RESOURCE_TYPE, cb6, m_leddResource);
    //ledg
    EntityHandler cb7 = bind(&IoTServer::LEDGEntityHandler, this, placeholders::_1);
    createResource(LEDG_RESOURCE_ENDPOINT, LEDG_RESOURCE_TYPE, cb7, m_ledgResource);
    //ledg
    EntityHandler cb8 = bind(&IoTServer::LEDGDEntityHandler, this, placeholders::_1);
    createResource(LEDGD_RESOURCE_ENDPOINT, LEDGD_RESOURCE_TYPE, cb8, m_ledgdResource);
    //moteur
    EntityHandler cb9 = bind(&IoTServer::MOTEUREntityHandler, this, placeholders::_1);
    createResource(MOTEUR_RESOURCE_ENDPOINT, MOTEUR_RESOURCE_TYPE, cb9, m_moteurResource);
}

void IoTServer::createResource(string Uri, string Type, EntityHandler Cb, OCResourceHandle& Handle)
{
    string resourceUri = Uri;
    string resourceType = Type;
    string resourceInterface = EDISON_RESOURCE_INTERFACE;
    uint8_t resourceFlag = OC_DISCOVERABLE | OC_OBSERVABLE;

    OCStackResult result = OCPlatform::registerResource(Handle, resourceUri, resourceType,
                                                        resourceInterface, Cb, resourceFlag);

    if (result != OC_STACK_OK)
        cerr << "Could not create " << Type << " resource" << endl;
    else
        cout << "Successfully created " << Type << " resource" << endl;
}

void IoTServer::putLEDRepresentation()
{
    int state = 0;
    m_ledRepresentation.getValue(LED_RESOURCE_KEY, state);
    SetOnboardLed(state);
    if (state == 0)
        cout << "Turned off LED" << endl;
    else if (state == 1)
        cout << "Turned on LED" << endl;
    else
        cerr << "Invalid request value" << endl;
}

OCRepresentation IoTServer::getLEDRepresentation()
{
    return m_ledRepresentation;
}

void IoTServer::putMOTEURRepresentation()
{
    int state = 0;
    m_moteurRepresentation.getValue(MOTEUR_RESOURCE_KEY, state);
    Setfoursteps(state);
    if (state == 0)
        cout << "Turned off Moteur" << endl;
    else if (state == 1)
        cout << "Turned on moteur" << endl;
    else
        cerr << "Invalid request value" << endl;
}

OCRepresentation IoTServer::getMOTEURRepresentation()
{
    return m_moteurRepresentation;
}

void IoTServer::putLEDDRepresentation()
{
    int state = 0;
    m_leddRepresentation.getValue(LEDD_RESOURCE_KEY, state);
    SetOnboardLedclinodroit(state);
}

OCRepresentation IoTServer::getLEDDRepresentation()
{
    return m_leddRepresentation;
}

void IoTServer::putLEDGRepresentation()
{
    int state = 0;
    m_ledgRepresentation.getValue(LEDG_RESOURCE_KEY, state);
    SetOnboardLedclinogauche(state);
}

OCRepresentation IoTServer::getLEDGRepresentation()
{
    return m_ledgRepresentation;
}
void IoTServer::putLEDGDRepresentation()
{
    int state = 0;
    m_ledgdRepresentation.getValue(LEDGD_RESOURCE_KEY, state);
    setOnboardfourclino(state);
}
OCRepresentation IoTServer::getLEDGDRepresentation()
{
    return m_ledgdRepresentation;
}
void IoTServer::temperatureObserverLoop()
{
    usleep(1500000);
    cout << "Temperature Observer Callback" << endl;
    shared_ptr<OCResourceResponse> resourceResponse(new OCResourceResponse());
    //resourceResponse->setErrorCode(200);
    resourceResponse->setResourceRepresentation(getTemperatureRepresentation(),
    EDISON_RESOURCE_INTERFACE);
    OCStackResult result = OCPlatform::notifyListOfObservers(m_temperatureResource,
                                                             m_temperatureObservers,
                                                             resourceResponse);
    if (result == OC_STACK_NO_OBSERVERS)
    {
        cout << "No more observers..Stopping observer loop..." << endl;
        m_temperatureObserverLoop->stop();
    }
}
void IoTServer::gazObserverLoop()
{
    usleep(1500000);
    cout << "Gaz Observer Callback" << endl;
    shared_ptr<OCResourceResponse> resourceResponse(new OCResourceResponse());
    //resourceResponse->setErrorCode(200);
    resourceResponse->setResourceRepresentation(getGazRepresentation(),
    EDISON_RESOURCE_INTERFACE);
    OCStackResult result = OCPlatform::notifyListOfObservers(m_gazResource,
                                                             m_gazObservers,
                                                             resourceResponse);
    if (result == OC_STACK_NO_OBSERVERS)
    {
        cout << "No more observers..Stopping observer loop..." << endl;
        m_gazObserverLoop->stop();
    }
}
void IoTServer::pressionObserverLoop()
{
    usleep(1500000);
    cout << "Pression Observer Callback" << endl;
    shared_ptr<OCResourceResponse> resourceResponse(new OCResourceResponse());
    //resourceResponse->setErrorCode(200);
    resourceResponse->setResourceRepresentation(getPressionRepresentation(),
    EDISON_RESOURCE_INTERFACE);
    OCStackResult result = OCPlatform::notifyListOfObservers(m_pressionResource,
                                                             m_pressionObservers,
                                                             resourceResponse);
    if (result == OC_STACK_NO_OBSERVERS)
    {
        cout << "No more observers..Stopping observer loop..." << endl;
        m_pressionObserverLoop->stop();
    }
}
void IoTServer::humidityObserverLoop()
{
    usleep(1500000);
    cout << "Humidity Observer Callback" << endl;
    shared_ptr<OCResourceResponse> resourceResponse(new OCResourceResponse());
    //resourceResponse->setErrorCode(200);
    resourceResponse->setResourceRepresentation(getHumidityRepresentation(),
    EDISON_RESOURCE_INTERFACE);
    OCStackResult result = OCPlatform::notifyListOfObservers(m_humidityResource,
                                                             m_humidityObservers,
                                                             resourceResponse);
    if (result == OC_STACK_NO_OBSERVERS)
    {
        cout << "No more observers..Stopping observer loop..." << endl;
        m_humidityObserverLoop->stop();
    }
}
   //OCRepresentation for Temperature
OCRepresentation IoTServer::getTemperatureRepresentation()
{
    m_temperatureRepresentation.setValue(TEMPERATURE_RESOURCE_KEY, GetTemperatureInC());
    return m_temperatureRepresentation;
}

OCEntityHandlerResult IoTServer::temperatureEntityHandler(shared_ptr<OCResourceRequest> Request)
{
    OCEntityHandlerResult result = OC_EH_ERROR;
    if (Request)
    {
        string requestType = Request->getRequestType();
        int requestFlag = Request->getRequestHandlerFlag();
        if (requestFlag & RequestHandlerFlag::RequestFlag)
        {
            auto Response = std::make_shared<OC::OCResourceResponse>();
            Response->setRequestHandle(Request->getRequestHandle());
            Response->setResourceHandle(Request->getResourceHandle());
            if (requestType == "GET")
            {
                cout << "GET request for temperature sensor reading" << endl;
                if (Response)
                {
                    //Response->setErrorCode(200);
                    Response->setResponseResult(OC_EH_OK);
                    Response->setResourceRepresentation(getTemperatureRepresentation());
                    if (OCPlatform::sendResponse(Response) == OC_STACK_OK)
                    {
                        result = OC_EH_OK;
                    }
                }
            }
            else
            {
                Response->setResponseResult(OC_EH_ERROR);
                OCPlatform::sendResponse(Response);
                cerr << "Unsupported request type" << endl;
                return result;
            }
        }
        if (requestFlag & RequestHandlerFlag::ObserverFlag)
        {
            ObservationInfo observationInfo = Request->getObservationInfo();
            if (ObserveAction::ObserveRegister == observationInfo.action)
            {
                cout << "Starting observer for temperature sensor" << endl;
                m_temperatureObservers.push_back(observationInfo.obsId);
                m_temperatureObserverLoop->start();
            }
            else if (ObserveAction::ObserveUnregister == observationInfo.action)
            {
                cout << "Stopping observer for temperature sensor" << endl;
                m_temperatureObservers.erase(
                        remove(m_temperatureObservers.begin(), m_temperatureObservers.end(),
                               observationInfo.obsId),
                        m_temperatureObservers.end());
                m_temperatureObserverLoop->stop();
            }
        }
    }
    return result;
}
  //OCRepresentation for Gaz
OCRepresentation IoTServer::getGazRepresentation()
{
    m_gazRepresentation.setValue(GAZ_RESOURCE_KEY, GetAverageGazRaw());
    return m_gazRepresentation;
}

OCEntityHandlerResult IoTServer::gazEntityHandler(shared_ptr<OCResourceRequest> Request)
{
    OCEntityHandlerResult result = OC_EH_ERROR;
    if (Request)
    {
        string requestType = Request->getRequestType();
        int requestFlag = Request->getRequestHandlerFlag();
        if (requestFlag & RequestHandlerFlag::RequestFlag)
        {
            auto Response = std::make_shared<OC::OCResourceResponse>();
            Response->setRequestHandle(Request->getRequestHandle());
            Response->setResourceHandle(Request->getResourceHandle());
            if (requestType == "GET")
            {
                cout << "GET request for MQ2_Gaz sensor reading" << endl;
                if (Response)
                {
                    //Response->setErrorCode(200);
                    Response->setResponseResult(OC_EH_OK);
                    Response->setResourceRepresentation(getGazRepresentation());
                    if (OCPlatform::sendResponse(Response) == OC_STACK_OK)
                    {
                        result = OC_EH_OK;
                    }
                }
            }
            else
            {
                Response->setResponseResult(OC_EH_ERROR);
                OCPlatform::sendResponse(Response);
                cerr << "Unsupported request type" << endl;
                return result;
            }
        }
        if (requestFlag & RequestHandlerFlag::ObserverFlag)
        {
            ObservationInfo observationInfo = Request->getObservationInfo();
            if (ObserveAction::ObserveRegister == observationInfo.action)
            {
                cout << "Starting observer for MQ2_Gaz sensor" << endl;
                m_gazObservers.push_back(observationInfo.obsId);
                m_gazObserverLoop->start();
            }
            else if (ObserveAction::ObserveUnregister == observationInfo.action)
            {
                cout << "Stopping observer for MQ2_Gaz sensor" << endl;
                m_gazObservers.erase(
                        remove(m_gazObservers.begin(), m_gazObservers.end(),
                               observationInfo.obsId),
                        m_gazObservers.end());
                m_gazObserverLoop->stop();
            }
        }
    }
    return result;
}
//OCRepresentation for humidity
OCRepresentation IoTServer::getHumidityRepresentation()
{
    m_humidityRepresentation.setValue(HUMIDITY_RESOURCE_KEY, GetAverageHumidityRaw());
    return m_humidityRepresentation;
}

OCEntityHandlerResult IoTServer::humidityEntityHandler(shared_ptr<OCResourceRequest> Request)
{
    OCEntityHandlerResult result = OC_EH_ERROR;
    if (Request)
    {
        string requestType = Request->getRequestType();
        int requestFlag = Request->getRequestHandlerFlag();
        if (requestFlag & RequestHandlerFlag::RequestFlag)
        {
            auto Response = std::make_shared<OC::OCResourceResponse>();
            Response->setRequestHandle(Request->getRequestHandle());
            Response->setResourceHandle(Request->getResourceHandle());
            if (requestType == "GET")
            {
                cout << "GET request for humidity sensor reading" << endl;
                if (Response)
                {
                    //Response->setErrorCode(200);
                    Response->setResponseResult(OC_EH_OK);
                    Response->setResourceRepresentation(getHumidityRepresentation());
                    if (OCPlatform::sendResponse(Response) == OC_STACK_OK)
                    {
                        result = OC_EH_OK;
                    }
                }
            }
            else
            {
                Response->setResponseResult(OC_EH_ERROR);
                OCPlatform::sendResponse(Response);
                cerr << "Unsupported request type" << endl;
                return result;
            }
        }
        if (requestFlag & RequestHandlerFlag::ObserverFlag)
        {
            ObservationInfo observationInfo = Request->getObservationInfo();
            if (ObserveAction::ObserveRegister == observationInfo.action)
            {
                cout << "Starting observer for humidity sensor" << endl;
                m_humidityObservers.push_back(observationInfo.obsId);
                m_humidityObserverLoop->start();
            }
            else if (ObserveAction::ObserveUnregister == observationInfo.action)
            {
                cout << "Stopping observer for humidity sensor" << endl;
                m_humidityObservers.erase(
                        remove(m_humidityObservers.begin(), m_humidityObservers.end(),
                               observationInfo.obsId),
                        m_humidityObservers.end());
                m_humidityObserverLoop->stop();
            }
        }
    }
    return result;
}
OCRepresentation IoTServer::getPressionRepresentation()
{
    m_pressionRepresentation.setValue(PRESSION_RESOURCE_KEY, GetAveragePressionRaw());
    return m_pressionRepresentation;
}

OCEntityHandlerResult IoTServer::pressionEntityHandler(shared_ptr<OCResourceRequest> Request)
{
    OCEntityHandlerResult result = OC_EH_ERROR;
    if (Request)
    {
        string requestType = Request->getRequestType();
        int requestFlag = Request->getRequestHandlerFlag();
        if (requestFlag & RequestHandlerFlag::RequestFlag)
        {
            auto Response = std::make_shared<OC::OCResourceResponse>();
            Response->setRequestHandle(Request->getRequestHandle());
            Response->setResourceHandle(Request->getResourceHandle());
            if (requestType == "GET")
            {
                cout << "GET request for DHT11 Pression sensor reading" << endl;
                if (Response)
                {
                    //Response->setErrorCode(200);
                    Response->setResponseResult(OC_EH_OK);
                    Response->setResourceRepresentation(getPressionRepresentation());
                    if (OCPlatform::sendResponse(Response) == OC_STACK_OK)
                    {
                        result = OC_EH_OK;
                    }
                }
            }
            else
            {
                Response->setResponseResult(OC_EH_ERROR);
                OCPlatform::sendResponse(Response);
                cerr << "Unsupported request type" << endl;
                return result;
            }
        }
        if (requestFlag & RequestHandlerFlag::ObserverFlag)
        {
            ObservationInfo observationInfo = Request->getObservationInfo();
            if (ObserveAction::ObserveRegister == observationInfo.action)
            {
                cout << "Starting observer for DHT11 Pression sensor" << endl;
                m_pressionObservers.push_back(observationInfo.obsId);
                m_pressionObserverLoop->start();
            }
            else if (ObserveAction::ObserveUnregister == observationInfo.action)
            {
                cout << "Stopping observer for DHT11 Pression sensor" << endl;
                m_pressionObservers.erase(
                        remove(m_pressionObservers.begin(), m_pressionObservers.end(),
                               observationInfo.obsId),
                        m_pressionObservers.end());
                m_pressionObserverLoop->stop();
            }
        }
    }
    return result;
}

OCEntityHandlerResult IoTServer::LEDEntityHandler(shared_ptr<OCResourceRequest> Request)
{
    OCEntityHandlerResult result = OC_EH_ERROR;
    if (Request)
    {
        string requestType = Request->getRequestType();
        int requestFlag = Request->getRequestHandlerFlag();
        if (requestFlag & RequestHandlerFlag::RequestFlag)
        {
            auto Response = std::make_shared<OC::OCResourceResponse>();
            Response->setRequestHandle(Request->getRequestHandle());
            Response->setResourceHandle(Request->getResourceHandle());
            if (requestType == "PUT")
            {
                cout << "PUT request for platform LED" << endl;
                OCRepresentation requestRep = Request->getResourceRepresentation();
                if (requestRep.hasAttribute(LED_RESOURCE_KEY))
                {
                    try
                    {
                        requestRep.getValue<int>(LED_RESOURCE_KEY);
                    }
                    catch (...)
                    {
                        Response->setResponseResult(OC_EH_ERROR);
                        OCPlatform::sendResponse(Response);
                        cerr << "Client sent invalid resource value type" << endl;
                        return result;
                    }
                }
                else
                {
                    Response->setResponseResult(OC_EH_ERROR);
                    OCPlatform::sendResponse(Response);
                    cerr << "Client sent invalid resource key" << endl;
                    return result;
                }
                m_ledRepresentation = requestRep;
                putLEDRepresentation();
                if (Response)
                {
                    //Response->setErrorCode(200);
                    Response->setResourceRepresentation(getLEDRepresentation());
                    Response->setResponseResult(OC_EH_OK);
                    if (OCPlatform::sendResponse(Response) == OC_STACK_OK)
                    {
                        result = OC_EH_OK;
                    }
                }
            }
            else if (requestType == "GET")
            {
                cout << "GET request for platform LED" << endl;
                if (Response)
                {
                    //Response->setErrorCode(200);
                    Response->setResourceRepresentation(getLEDRepresentation());
                    Response->setResponseResult(OC_EH_OK);
                    if (OCPlatform::sendResponse(Response) == OC_STACK_OK)
                    {
                        result = OC_EH_OK;
                    }
                }
            }
            else
            {
                Response->setResponseResult(OC_EH_ERROR);
                OCPlatform::sendResponse(Response);
                cerr << "Unsupported request type" << endl;
            }
        }
    }
    return result;
}


OCEntityHandlerResult IoTServer::LEDDEntityHandler(shared_ptr<OCResourceRequest> Request)
{
    OCEntityHandlerResult result = OC_EH_ERROR;
    if (Request)
    {
        string requestType = Request->getRequestType();
        int requestFlag = Request->getRequestHandlerFlag();
        if (requestFlag & RequestHandlerFlag::RequestFlag)
        {
            auto Response = std::make_shared<OC::OCResourceResponse>();
            Response->setRequestHandle(Request->getRequestHandle());
            Response->setResourceHandle(Request->getResourceHandle());
            if (requestType == "PUT")
            {
                cout << "PUT request for platform LED" << endl;
                OCRepresentation requestRep = Request->getResourceRepresentation();
                if (requestRep.hasAttribute(LEDD_RESOURCE_KEY))
                {
                    try
                    {
                        requestRep.getValue<int>(LEDD_RESOURCE_KEY);
                    }
                    catch (...)
                    {
                        Response->setResponseResult(OC_EH_ERROR);
                        OCPlatform::sendResponse(Response);
                        cerr << "Client sent invalid resource value type" << endl;
                        return result;
                    }
                }
                else
                {
                    Response->setResponseResult(OC_EH_ERROR);
                    OCPlatform::sendResponse(Response);
                    cerr << "Client sent invalid resource key" << endl;
                    return result;
                }
                m_leddRepresentation = requestRep;
                putLEDDRepresentation();
                if (Response)
                {
                    //Response->setErrorCode(200);
                    Response->setResourceRepresentation(getLEDDRepresentation());
                    Response->setResponseResult(OC_EH_OK);
                    if (OCPlatform::sendResponse(Response) == OC_STACK_OK)
                    {
                        result = OC_EH_OK;
                    }
                }
            }
            else if (requestType == "GET")
            {
                cout << "GET request for platform LED" << endl;
                if (Response)
                {
                    //Response->setErrorCode(200);
                    Response->setResourceRepresentation(getLEDDRepresentation());
                    Response->setResponseResult(OC_EH_OK);
                    if (OCPlatform::sendResponse(Response) == OC_STACK_OK)
                    {
                        result = OC_EH_OK;
                    }
                }
            }
            else
            {
                Response->setResponseResult(OC_EH_ERROR);
                OCPlatform::sendResponse(Response);
                cerr << "Unsupported request type" << endl;
            }
        }
    }
    return result;
}

OCEntityHandlerResult IoTServer::LEDGEntityHandler(shared_ptr<OCResourceRequest> Request)
{
    OCEntityHandlerResult result = OC_EH_ERROR;
    if (Request)
    {
        string requestType = Request->getRequestType();
        int requestFlag = Request->getRequestHandlerFlag();
        if (requestFlag & RequestHandlerFlag::RequestFlag)
        {
            auto Response = std::make_shared<OC::OCResourceResponse>();
            Response->setRequestHandle(Request->getRequestHandle());
            Response->setResourceHandle(Request->getResourceHandle());
            if (requestType == "PUT")
            {
                cout << "PUT request for platform LED" << endl;
                OCRepresentation requestRep = Request->getResourceRepresentation();
                if (requestRep.hasAttribute(LEDG_RESOURCE_KEY))
                {
                    try
                    {
                        requestRep.getValue<int>(LEDG_RESOURCE_KEY);
                    }
                    catch (...)
                    {
                        Response->setResponseResult(OC_EH_ERROR);
                        OCPlatform::sendResponse(Response);
                        cerr << "Client sent invalid resource value type" << endl;
                        return result;
                    }
                }
                else
                {
                    Response->setResponseResult(OC_EH_ERROR);
                    OCPlatform::sendResponse(Response);
                    cerr << "Client sent invalid resource key" << endl;
                    return result;
                }
                m_ledgRepresentation = requestRep;
                putLEDGRepresentation();
                if (Response)
                {
                    //Response->setErrorCode(200);
                    Response->setResourceRepresentation(getLEDGRepresentation());
                    Response->setResponseResult(OC_EH_OK);
                    if (OCPlatform::sendResponse(Response) == OC_STACK_OK)
                    {
                        result = OC_EH_OK;
                    }
                }
            }
            else if (requestType == "GET")
            {
                cout << "GET request for platform LED" << endl;
                if (Response)
                {
                    //Response->setErrorCode(200);
                    Response->setResourceRepresentation(getLEDGRepresentation());
                    Response->setResponseResult(OC_EH_OK);
                    if (OCPlatform::sendResponse(Response) == OC_STACK_OK)
                    {
                        result = OC_EH_OK;
                    }
                }
            }
            else
            {
                Response->setResponseResult(OC_EH_ERROR);
                OCPlatform::sendResponse(Response);
                cerr << "Unsupported request type" << endl;
            }
        }
    }
    return result;
}
OCEntityHandlerResult IoTServer::LEDGDEntityHandler(shared_ptr<OCResourceRequest> Request)
{
    OCEntityHandlerResult result = OC_EH_ERROR;
    if (Request)
    {
        string requestType = Request->getRequestType();
        int requestFlag = Request->getRequestHandlerFlag();
        if (requestFlag & RequestHandlerFlag::RequestFlag)
        {
            auto Response = std::make_shared<OC::OCResourceResponse>();
            Response->setRequestHandle(Request->getRequestHandle());
            Response->setResourceHandle(Request->getResourceHandle());
            if (requestType == "PUT")
            {
                cout << "PUT request for platform LEDGD" << endl;
                OCRepresentation requestRep = Request->getResourceRepresentation();
                if (requestRep.hasAttribute(LEDGD_RESOURCE_KEY))
                {
                    try
                    {
                        requestRep.getValue<int>(LEDGD_RESOURCE_KEY);
                    }
                    catch (...)
                    {
                        Response->setResponseResult(OC_EH_ERROR);
                        OCPlatform::sendResponse(Response);
                        cerr << "Client sent invalid resource value type" << endl;
                        return result;
                    }
                }
                else
                {
                    Response->setResponseResult(OC_EH_ERROR);
                    OCPlatform::sendResponse(Response);
                    cerr << "Client sent invalid resource key" << endl;
                    return result;
                }
                m_ledgdRepresentation = requestRep;
                putLEDGDRepresentation();
                if (Response)
                {
                    //Response->setErrorCode(200);
                    Response->setResourceRepresentation(getLEDGDRepresentation());
                    Response->setResponseResult(OC_EH_OK);
                    if (OCPlatform::sendResponse(Response) == OC_STACK_OK)
                    {
                        result = OC_EH_OK;
                    }
                }
            }
            else if (requestType == "GET")
            {
                cout << "GET request for platform LED" << endl;
                if (Response)
                {
                    //Response->setErrorCode(200);
                    Response->setResourceRepresentation(getLEDGDRepresentation());
                    Response->setResponseResult(OC_EH_OK);
                    if (OCPlatform::sendResponse(Response) == OC_STACK_OK)
                    {
                        result = OC_EH_OK;
                    }
                }
            }
            else
            {
                Response->setResponseResult(OC_EH_ERROR);
                OCPlatform::sendResponse(Response);
                cerr << "Unsupported request type" << endl;
            }
        }
    }
    return result;
}

OCEntityHandlerResult IoTServer::MOTEUREntityHandler(shared_ptr<OCResourceRequest> Request)
{
    OCEntityHandlerResult result = OC_EH_ERROR;
    if (Request)
    {
        string requestType = Request->getRequestType();
        int requestFlag = Request->getRequestHandlerFlag();
        if (requestFlag & RequestHandlerFlag::RequestFlag)
        {
            auto Response = std::make_shared<OC::OCResourceResponse>();
            Response->setRequestHandle(Request->getRequestHandle());
            Response->setResourceHandle(Request->getResourceHandle());
            if (requestType == "PUT")
            {
                cout << "PUT request for platform MOTEUR" << endl;
                OCRepresentation requestRep = Request->getResourceRepresentation();
                if (requestRep.hasAttribute(MOTEUR_RESOURCE_KEY))
                {
                    try
                    {
                        requestRep.getValue<int>(MOTEUR_RESOURCE_KEY);
                    }
                    catch (...)
                    {
                        Response->setResponseResult(OC_EH_ERROR);
                        OCPlatform::sendResponse(Response);
                        cerr << "Client sent invalid resource value type" << endl;
                        return result;
                    }
                }
                else
                {
                    Response->setResponseResult(OC_EH_ERROR);
                    OCPlatform::sendResponse(Response);
                    cerr << "Client sent invalid resource key" << endl;
                    return result;
                }
                m_moteurRepresentation = requestRep;
                putMOTEURRepresentation();
                if (Response)
                {
                    //Response->setErrorCode(200);
                    Response->setResourceRepresentation(getMOTEURRepresentation());
                    Response->setResponseResult(OC_EH_OK);
                    if (OCPlatform::sendResponse(Response) == OC_STACK_OK)
                    {
                        result = OC_EH_OK;
                    }
                }
            }
            else if (requestType == "GET")
            {
                cout << "GET request for platform MOTEUR" << endl;
                if (Response)
                {
                    //Response->setErrorCode(200);
                    Response->setResourceRepresentation(getMOTEURRepresentation());
                    Response->setResponseResult(OC_EH_OK);
                    if (OCPlatform::sendResponse(Response) == OC_STACK_OK)
                    {
                        result = OC_EH_OK;
                    }
                }
            }
            else
            {
                Response->setResponseResult(OC_EH_ERROR);
                OCPlatform::sendResponse(Response);
                cerr << "Unsupported request type" << endl;
            }
        }
    }
    return result;
}

int quit = 0;

void handle_signal(int signal)
{
    quit = 1;
}

int main()
{
    struct sigaction sa;
    sigfillset(&sa.sa_mask);
    sa.sa_flags = 0;
    sa.sa_handler = handle_signal;
    sigaction(SIGINT, &sa, NULL);
    cout << "Press Ctrl-C to quit...." << endl;
    IoTServer server;
    do
    {
        usleep(2000000);
    }
    while (quit != 1);
    return 0;
}

