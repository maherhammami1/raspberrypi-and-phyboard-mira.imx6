/****************************************************************************
** Meta object code from reading C++ file 'processif.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../actia-qtclient/files/actia-qtclient/actia-qtclient/processif.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'processif.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ProcessIf_t {
    QByteArrayData data[20];
    char stringdata0[177];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ProcessIf_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ProcessIf_t qt_meta_stringdata_ProcessIf = {
    {
QT_MOC_LITERAL(0, 0, 9), // "ProcessIf"
QT_MOC_LITERAL(1, 10, 8), // "hasError"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 12), // "errorMessage"
QT_MOC_LITERAL(4, 33, 11), // "led1Changed"
QT_MOC_LITERAL(5, 45, 6), // "newVal"
QT_MOC_LITERAL(6, 52, 11), // "led2Changed"
QT_MOC_LITERAL(7, 64, 11), // "led3Changed"
QT_MOC_LITERAL(8, 76, 12), // "gpio1Changed"
QT_MOC_LITERAL(9, 89, 12), // "gpio2Changed"
QT_MOC_LITERAL(10, 102, 12), // "gpio3Changed"
QT_MOC_LITERAL(11, 115, 12), // "errorChanged"
QT_MOC_LITERAL(12, 128, 9), // "timerTick"
QT_MOC_LITERAL(13, 138, 4), // "led1"
QT_MOC_LITERAL(14, 143, 4), // "led2"
QT_MOC_LITERAL(15, 148, 4), // "led3"
QT_MOC_LITERAL(16, 153, 5), // "gpio1"
QT_MOC_LITERAL(17, 159, 5), // "gpio2"
QT_MOC_LITERAL(18, 165, 5), // "gpio3"
QT_MOC_LITERAL(19, 171, 5) // "error"

    },
    "ProcessIf\0hasError\0\0errorMessage\0"
    "led1Changed\0newVal\0led2Changed\0"
    "led3Changed\0gpio1Changed\0gpio2Changed\0"
    "gpio3Changed\0errorChanged\0timerTick\0"
    "led1\0led2\0led3\0gpio1\0gpio2\0gpio3\0error"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ProcessIf[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       7,   82, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x06 /* Public */,
       4,    1,   62,    2, 0x06 /* Public */,
       6,    1,   65,    2, 0x06 /* Public */,
       7,    1,   68,    2, 0x06 /* Public */,
       8,    1,   71,    2, 0x06 /* Public */,
       9,    1,   74,    2, 0x06 /* Public */,
      10,    1,   77,    2, 0x06 /* Public */,
      11,    0,   80,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      12,    0,   81,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,

 // properties: name, type, flags
      13, QMetaType::Int, 0x00495103,
      14, QMetaType::Int, 0x00495103,
      15, QMetaType::Int, 0x00495103,
      16, QMetaType::Int, 0x00495001,
      17, QMetaType::Int, 0x00495001,
      18, QMetaType::Int, 0x00495001,
      19, QMetaType::QString, 0x00495001,

 // properties: notify_signal_id
       1,
       2,
       3,
       4,
       5,
       6,
       7,

       0        // eod
};

void ProcessIf::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ProcessIf *_t = static_cast<ProcessIf *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->hasError((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->led1Changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->led2Changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->led3Changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->gpio1Changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->gpio2Changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->gpio3Changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->errorChanged(); break;
        case 8: _t->timerTick(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (ProcessIf::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProcessIf::hasError)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (ProcessIf::*_t)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProcessIf::led1Changed)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (ProcessIf::*_t)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProcessIf::led2Changed)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (ProcessIf::*_t)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProcessIf::led3Changed)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (ProcessIf::*_t)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProcessIf::gpio1Changed)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (ProcessIf::*_t)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProcessIf::gpio2Changed)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (ProcessIf::*_t)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProcessIf::gpio3Changed)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (ProcessIf::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProcessIf::errorChanged)) {
                *result = 7;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        ProcessIf *_t = static_cast<ProcessIf *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->getLed1(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->getLed2(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->getLed3(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->getGpio1(); break;
        case 4: *reinterpret_cast< int*>(_v) = _t->getGpio2(); break;
        case 5: *reinterpret_cast< int*>(_v) = _t->getGpio3(); break;
        case 6: *reinterpret_cast< QString*>(_v) = _t->getError(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        ProcessIf *_t = static_cast<ProcessIf *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setLed1(*reinterpret_cast< int*>(_v)); break;
        case 1: _t->setLed2(*reinterpret_cast< int*>(_v)); break;
        case 2: _t->setLed3(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject ProcessIf::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ProcessIf.data,
      qt_meta_data_ProcessIf,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *ProcessIf::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ProcessIf::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ProcessIf.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int ProcessIf::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 7;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void ProcessIf::hasError(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ProcessIf::led1Changed(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ProcessIf::led2Changed(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void ProcessIf::led3Changed(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void ProcessIf::gpio1Changed(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void ProcessIf::gpio2Changed(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void ProcessIf::gpio3Changed(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void ProcessIf::errorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
